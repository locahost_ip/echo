package com.echo.boot.annotation;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.function.Consumer;

/**
 * Created with IntelliJ IDEA
 * Created By CQ
 * Date: 2020/7/3
 * Time: 21:18
 *
 * @author CQ
 */
public class Main {

    public static void main(String[] args) throws ClassNotFoundException, InstantiationException, IllegalAccessException {
        Class<?> aClass = Class.forName("com.echo.boot.pojo.Anno");

        // 获取注解
        MyAnnotation annotation = aClass.getAnnotation(MyAnnotation.class);

        // 获取注解属性
        System.out.println(annotation.name());
        System.out.println(annotation.age());
        System.out.println(annotation.color());

        junitTest();
    }

    static void junitTest() throws IllegalAccessException, InstantiationException {
        Class<junitAnnoTest> aClass = junitAnnoTest.class;
        junitAnnoTest instance = aClass.newInstance();

        Method[] methods = aClass.getMethods();

        ArrayList<Method> beforeMethod = new ArrayList<>();
        ArrayList<Method> testMethod = new ArrayList<>();
        ArrayList<Method> afterMethod = new ArrayList<>();
        for (Method method : methods) {
            if (method.isAnnotationPresent(MyBefore.class)) {
                beforeMethod.add(method);
            }
            if (method.isAnnotationPresent(MyTest.class)) {
                testMethod.add(method);
            }
            if (method.isAnnotationPresent(MyAfter.class)) {
                afterMethod.add(method);
            }
        }

        beforeMethod.forEach(method -> {
            try {
                method.invoke(instance);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
        });
        testMethod.forEach(method -> {
            try {
                method.invoke(instance);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
        });

        afterMethod.forEach(method -> {
            try {
                method.invoke(instance);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
        });


    }

}
