package com.echo.boot.annotation;

/**
 * Created with IntelliJ IDEA
 * Created By CQ
 * Date: 2020/7/9
 * Time: 21:43
 *
 * @author Administrator
 */
public @interface MyRateLimiter {

    /**
     * 已每秒为单位的速率往令牌桶中添加令牌
     *
     * @return
     */
    double permitsPerSecond() default 1.0;

    /**
     * 在规定的毫秒时间内,如果没有获取到令牌的话,则直接走服务降级
     *
     * @return
     */
    long timeout();


}
