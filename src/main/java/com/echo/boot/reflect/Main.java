package com.echo.boot.reflect;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Main {
    public static void main(String[] args) throws InvocationTargetException, IllegalAccessException {

//        ReflectClass reflectClass = new ReflectClass();
//        Method[] methods = ReflectClass.class.getMethods();
//        for(Method method : methods){
//            if("test1".equals(method.getName())){
//                Class<?>[] types = method.getParameterTypes();
//                method.invoke(reflectClass,"aa","bb",55);
//            }
//        }

    }

    public void testFlect(Class<? extends ParentReflectClass> clazz) throws IllegalAccessException, InstantiationException {
        ParentReflectClass parentReflectClass = clazz.newInstance();
        parentReflectClass.sayHe();
    }
}
