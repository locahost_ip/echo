package com.echo.boot.leetcode.链表;

/**
 * Created with IntelliJ IDEA
 * Created By CQ
 * Date: 2020/5/3
 * Time: 12:16
 */
public class _140_是否有环链表 {
    public static void main(String[] args) {
        // 快慢指针
    }

    public boolean hasCycle(ListNode head) {
        ListNode slow = head;
        ListNode fast = head.next;
        while (fast != null && fast.next != null) {
            slow = slow.next;
            fast = fast.next.next;
            if (slow == fast) {
                return true;
            }
        }
        return false;
    }
}
