package com.echo.boot.structure.tree.binarytree;

/**
 * Created with IntelliJ IDEA
 * Created By CQ
 * Date: 2020/5/15
 * Time: 10:02
 */
public abstract class Visitor<E> {
    /**
     * stop 是否停止遍历
     */
    boolean stop;

    /**
     * @author CQ
     * @DESCRIPTION: 根据返回的二叉树的元素, 做自己的逻辑操作
     * @params: [element] 二叉树的元素
     * @return: boolean 是否停止遍历
     * @Date: 2020/5/15 10:06
     * @Modified By: CQ
     */
    public abstract boolean visit(E element);

}

