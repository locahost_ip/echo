package com.echo.boot.design.turing.adapter.v1;

/**
 * Created with IntelliJ IDEA
 * Created By CQ
 * Date: 2020/8/22
 * Time: 20:04
 * class Adapter
 */
public class AdapterTest1 {
    public static void main(String[] args) {
        Adapter adapter = new Adapter();
        adapter.outPut5v();
    }
}

class Adaptee {
    public int outPut220v() {
        return 220;
    }
}


interface Target {
    /**
     * 输出 5v 电;
     *
     * @return
     */
    int outPut5v();
}

class Adapter extends Adaptee implements Target {

    @Override
    public int outPut5v() {
        int i = outPut220v();
        System.out.println(String.format("原始电压： %d v -> 输出电压 %d v", i, 5));
        return 5;
    }
}
