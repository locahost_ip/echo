package com.echo.boot.design.turing.strategy;

/**
 * Created with IntelliJ IDEA
 * Created By CQ
 * Date: 2020/8/22
 * Time: 23:13
 * 策略模式
 * 定义了算法族,分别封装，让他们之间可以相互替换，此模式的变化独立于算法的使用者
 *
 * @author CQ
 */
public class StrategyTest {
    public static void main(String[] args) {
        Zombie zombie = new NormalZombie();
        zombie.display();
        zombie.move();
        zombie.attack();
        zombie.setAttackable(new HitAttack());
        zombie.attack();
    }
}

interface Moveable {
    public void move();
}

interface Attackable {
    public void attack();
}

abstract class Zombie {
    protected Moveable moveable;
    protected Attackable attackable;

    Zombie(Moveable moveable, Attackable attackable) {
        this.attackable = attackable;
        this.moveable = moveable;
    }

    abstract void display();

    abstract void move();

    abstract void attack();

    public Moveable getMoveable() {
        return moveable;
    }

    public void setMoveable(Moveable moveable) {
        this.moveable = moveable;
    }

    public Attackable getAttackable() {
        return attackable;
    }

    public void setAttackable(Attackable attackable) {
        this.attackable = attackable;
    }
}

class StepByStepMove implements Moveable {

    @Override
    public void move() {
        System.out.println("一步一步");
    }
}

class BiteAttack implements Attackable {

    @Override
    public void attack() {
        System.out.println("咬");
    }
}

class HitAttack implements Attackable {

    @Override
    public void attack() {
        System.out.println("用头撞");
    }
}

class NormalZombie extends Zombie {
    public NormalZombie() {
        super(new StepByStepMove(), new BiteAttack());
    }

    public NormalZombie(Moveable moveable, Attackable attackable) {
        super(moveable, attackable);
    }

    @Override
    void display() {
        System.out.println("普通僵尸");
    }

    @Override
    void move() {
        moveable.move();
    }

    @Override
    void attack() {
        attackable.attack();
    }
}


