package com.echo.boot.design.turing.decorator;

/**
 * Created with IntelliJ IDEA
 * Created By CQ
 * Date: 2020/8/22
 * Time: 22:48
 * 装饰器模式
 * 扩展一个类的功能或者给一个类添加附属职责
 * 优点
 * 不改变原有对象的情况下给一个对象扩展功能
 * 使用不同的组合可以实现不同的效果
 * 符合开闭原则
 * <p>
 * 在不改变原有的对象的基础上,将功能附加到对象上
 */
public class decoratorTest {
    public static void main(String[] args) {
        // 拍照
        Component component = new ConcreteComponent();
        // 美颜
        ConcreteDecorator decorator = new ConcreteDecorator(component);
        // 滤镜
        ConcreteDecorator2 decorator2 = new ConcreteDecorator2(decorator);
        decorator2.operation();
    }
}

interface Component {
    public void operation();
}

class ConcreteComponent implements Component {

    @Override
    public void operation() {
        System.out.println("拍照。");
    }
}

abstract class Decorator implements Component {
    Component component;

    public Decorator(Component component) {
        this.component = component;
    }
}

class ConcreteDecorator extends Decorator {

    public ConcreteDecorator(Component component) {
        super(component);
    }

    @Override
    public void operation() {
        component.operation();
        System.out.println("美颜。");
    }
}


class ConcreteDecorator2 extends Decorator {

    public ConcreteDecorator2(Component component) {
        super(component);
    }

    @Override
    public void operation() {
        component.operation();
        System.out.println("滤镜。");
    }
}

