package com.echo.boot.pojo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author Administrator
 */
@Getter
@Setter
@ApiModel(value = "User实体", description = "用户实体类")
public class User implements Serializable {

    @ApiModelProperty(value = "用户ID", required = false, dataType = "Integer")
    private Integer id;

    @ApiModelProperty(value = "用户名", required = true, dataType = "String")
    private String username;

    @ApiModelProperty(value = "年龄", required = true, example = "19", dataType = "Integer")
    private Integer age;

    @ApiModelProperty(value = "密码", required = true, dataType = "String")
    private String password;

    @ApiModelProperty(value = "性别", required = false, example = "0", dataType = "Byte")
    private Byte gender;

    @ApiModelProperty(value = "爱好", required = false, dataType = "String")
    private String hobbies;

    @ApiModelProperty(value = "生日", required = false, dataType = "Date")
    private LocalDateTime birthday;

    @ApiModelProperty(value = "地址", required = false, dataType = "String")
    private String address;

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", age=" + age +
                ", password='" + password + '\'' +
                ", gender=" + gender +
                ", hobbies='" + hobbies + '\'' +
                ", birthday=" + birthday +
                ", address='" + address + '\'' +
                '}';
    }
}