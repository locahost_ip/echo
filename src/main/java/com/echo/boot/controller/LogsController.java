package com.echo.boot.controller;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * Created with IntelliJ IDEA
 * Created By CQ
 * Date: 2020/1/21
 * Time: 21:20
 * springboot 2.0 整合 log4j2
 * springboot 2.0 默认整合 logback
 * log4j,log4j2,logback,
 * 了解日志体系，记住每个日志的配置详细注释
 * 备注备份每个日志文件配置
 */
@SpringBootTest
public class LogsController {

    @Test
    public void logTest() {
        Logger logger = LoggerFactory.getLogger(LogsController.class);
        try {
            for (int i = 1; i < 5000; i++) {
                logger.info("测试 TimeBasedTriggeringPolicy 滚动策略 时间维度到 秒;由 filePattern 决定 %d{yyyy-MM-dd HH:mm:ss} 全部写到 ALL_ROLLING_FILE 里");
                logger.info("第{}次", i);
                logger.trace("trace level");
                logger.debug("debug level");
                logger.info("info level");
                logger.warn("warn level");
                logger.error("error level");
                Thread.sleep(2000);
            }
        } catch (Exception e) {
            e.getMessage();
        }
    }

}

