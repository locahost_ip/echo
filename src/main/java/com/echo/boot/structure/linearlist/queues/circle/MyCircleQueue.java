package com.echo.boot.structure.linearlist.queues.circle;

/**
 * Created with IntelliJ IDEA
 * Created By CQ
 * Date: 2020/5/8
 * Time: 9:40
 * 循环队列
 * 环形队列
 * 从 队尾 添加元素 ，对头取出元素
 * 底层用动态数组实现
 * front 指向 第一个元素
 *
 * @author CQ
 */
public class MyCircleQueue<E> {
    private int front;
    private int size;
    private E[] elements;
    private static final int DEFAULT_CAPACITY = 10;

    public MyCircleQueue() {
        elements = (E[]) new Object[DEFAULT_CAPACITY];
    }

    public int sise() {
        return size;
    }

    public boolean isEmpty() {
        return size == 0;
    }

    public void enQueue(E element) {
        // 入队
        /*
         *  front
         *  │
         * ┌─┬─┬─┬─┬─┬─┬─┬─┬─┬─┐
         * │0│1│2│3│4│5│6│7│8│9│
         * └─┴─┴─┴─┴─┴─┴─┴─┴─┴─┘
         *                  front
         *                  │
         * ┌─┬─┬─┬─┬─┬─┬─┬─┬─┬─┐
         * │ │ │ │ │ │ │ │ │8│9│
         * └─┴─┴─┴─┴─┴─┴─┴─┴─┴─┘
         *  │
         *  添加 element
         * (8 + 2) % 10
         *  元素出队(从头部出队),front往后移动,front++; 如果移至到末尾几个元素
         *  当队列入队(从尾部入队); 真实索引的获取?
         *  //front 指向最后一位时,添加的元素应该往前放。索引怎么获得?
         *  //(front + size(实际个数)) 取模 容量 DEFAULT_CAPACITY
         *  索引： (front + size) % elements.length
         *  真实索引的优化 :  n % m 在 index() 处优化
         */
        // 扩容
        ensureCapacity(size + 1);
        elements[index(size)] = element;
        size++;
    }

    public E deQueue() {
        // 出队 第一个
        E frontElement = elements[front];
        // 当前元素清空
        elements[front] = null;

        /* 出队
         *  front
         *  │
         * ┌─┬─┬─┬─┬─┬─┬─┬─┬─┬─┐
         * │0│1│2│3│4│5│6│7│8│9│
         * └─┴─┴─┴─┴─┴─┴─┴─┴─┴─┘
         *         rear      front
         *          │         │ --->
         * ┌─┬─┬─┬─┬─┬─┬─┬─┬─┬─┐
         * │6│5│4│3│2│ │ │ │ │9│
         * └─┴─┴─┴─┴─┴─┴─┴─┴─┴─┘
         *  │
         *  front
         *  元素出队(从头部出队),front往后移动,front++; 如果移至到末尾;继续出队; frong 的位置怎么确认 ?
         *  真实索引的获取： (front + 1) % element.length; 取模
         *  (9 + 1) % 10 = 0;
         */
        // front = index(front+1);
        // 指针向后移一位
        front = index(1);
        size--;
        return frontElement;
    }

    public E front() {
        return elements[front];
    }

    private int index(int index) {
        // 为了循环
        // 通过当前索引获取真实的索引

//       return (front + index) % elements.length;
        index += front;
        if (index < (elements.length << 1)) {
            return index - (index >= elements.length ? elements.length : 0);
        }
        return index % elements.length;
    }

    private void ensureCapacity(int capacity) {
        int oldCapacity = elements.length;
        if (oldCapacity >= capacity) {
            return;
        }
//        int newCapacity = oldCapacity << 2;
        // 1.5 倍
        int newCapacity = oldCapacity + (oldCapacity >> 1);
        E[] newElements = (E[]) new Object[newCapacity];
//        System.arraycopy(elements, 0, newElements, 0, elements.length);
        // 把原来的元素复制到新的数组中
        // 从front 的索引开始 移动 ; 怎么获取真实的索引:  （i + front） % element.length;
        for (int i = 0; i < size; i++) {
            newElements[i] = elements[index(i)];
        }
        elements = newElements;
        front = 0;
        System.out.println(capacity + "扩容为" + newCapacity + " size " + size);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("capacity=").append(elements.length);
        builder.append(", front=").append(front);
        builder.append(", size=").append(size);
        builder.append(", [");
        for (int i = 0; i < elements.length; i++) {
            if (i != 0) {
                builder.append(", ");
            }
            builder.append(elements[i]);
        }
        builder.append("]");
        return builder.toString();
    }

    public void clear() {
        for (int i = 0; i < size; i++) {
            elements[index(i)] = null;
        }
        size = 0;
        front = 0;
    }
}
