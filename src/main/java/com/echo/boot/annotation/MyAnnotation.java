package com.echo.boot.annotation;

import com.echo.boot.enumeration.Color;

import java.lang.annotation.*;

/**
 * Created with IntelliJ IDEA
 * Created By CQ
 * Date: 2020/7/3
 * Time: 11:34
 * <p>
 * <p>
 * 注解本质上就是一个接口
 * 默认继承 java.lang.annotation 接口；
 * 属性返回值类型
 * 基本数据类型 String 枚举 注解 以上类型的数组；
 * <p>
 * 定义了属性,在使用的时候要给属性赋值
 * 1,如果定义属性时，使用default 关键字给属性默认初始化值，使用注解时，可以不进行属性赋值
 * 2,如果只有一个属性，且属性名称 为 value; 则 value 可以省略，直接定义值即可
 * 3,数组赋值时，使用{} 包裹，如果只有一个值，则{} 可以省略;
 *
 * @author Administrator
 * @Target 描述注解能够作用的位置
 * @Rententon 描述注解被保留那个阶段
 * @Documented 描述注解是否被抽取到 API 文档中
 * @Inherited 描述是否被子类继承
 */
@Documented
@Target({ElementType.METHOD, ElementType.TYPE, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface MyAnnotation {

    @OnAnnotation(name = "Echo")
    String name();

    int age();

    Color color();

    OnAnnotation onAnnotation();

    String[] strs();

}
