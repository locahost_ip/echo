package com.echo.boot.lambda;

/**
 * Created with IntelliJ IDEA
 * Created By CQ
 * Date: 2020/4/8
 * Time: 17:00
 */
public class LambdeDemo {
    public static void main(String[] args) {
        // 1.
        Runnable r1 = new Runnable() {
            @Override
            public void run() {
                System.out.println("do something ...");
            }
        };
        Thread t1 = new Thread(r1);

        //2
        new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("do something ...");
            }
        }).start();


        // 3 lambda
        new Thread(() -> {
            System.out.println("do something ....");
        }).start();

        // 4 lambde简洁版

        new Thread(() -> System.out.println("do something ..."));


    }
}
