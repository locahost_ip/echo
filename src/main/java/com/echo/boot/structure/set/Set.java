package com.echo.boot.structure.set;

import com.echo.boot.structure.tree.binarytree.Visitor;

/**
 * Created with IntelliJ IDEA
 * Created By CQ
 * Date: 2020/6/2
 * Time: 17:23
 */
public interface Set<E> {
    /**
     * 集合的长度
     *
     * @return
     */
    int size();

    /**
     * 集合是否为空
     *
     * @return
     */
    boolean isEmpty();

    /**
     * 清空集合
     */
    void clear();

    /**
     * 是否包含传入的元素
     *
     * @param element
     * @return
     */
    boolean contains(E element);

    /**
     * 添加元素
     *
     * @param element
     */
    void add(E element);

    /**
     * 移除元素
     *
     * @param element
     */
    void remove(E element);

    /**
     * 提供遍历元素
     *
     * @param visitor
     */
    void traversal(Visitor<E> visitor);

}
