package com.echo.boot.others;

/**
 * Created with IntelliJ IDEA
 * Created By CQ
 * Date: 2020/7/5
 * Time: 21:23
 * finally 什么时候执行
 */
public class ShowFinally {
    public static void main(String[] args) {
        int b = int2();
        System.out.println(b);
    }

    public static int int1() {
        int b = 20;
        try {
            System.out.println("try block");
            b += 80;
            return b;
        } catch (Exception e) {
            System.out.println("catch block");
        } finally {
            System.out.println("finally block");
            if (b > 25) {
                System.out.println("b > 25 b= " + b);
            }
            b = 123;
        }
        return 200;
    }

    public static int int2() {
        int b = 20;
        try {
            System.out.println("try block");
            b = b / 0;
            return b += 80;
        } catch (Exception e) {
            System.out.println("catch block");
            return b += 15;
        } finally {
            System.out.println("finally block");
            if (b > 25) {
                System.out.println("b > 25 b= " + b);
            }
            b += 50;
        }
//        return 200;
    }
}
