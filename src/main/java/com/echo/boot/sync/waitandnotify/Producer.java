package com.echo.boot.sync.waitandnotify;

/**
 * Created with IntelliJ IDEA
 * Created By CQ
 * Date: 2020/9/13
 * Time: 22:01
 */
public class Producer implements Runnable {

    private Drop drop;

    public Producer(Drop drop) {
        this.drop = drop;
    }

    @Override
    public void run() {
        String foods[] = {"beef", "bread", "apple", "cookie"};

        for (int i = 0; i < foods.length; i++) {
            try {
                Thread.sleep(1000); // 生产者生产食物2秒
            } catch (InterruptedException e) {
            }
            // 将foods[i]传递给消费者
            drop.add(foods[i]);
        }
        // 告诉消费者：不会再生产任何东西了
        drop.add(null);
    }
}
