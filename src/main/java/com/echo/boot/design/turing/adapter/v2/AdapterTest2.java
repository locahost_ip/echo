package com.echo.boot.design.turing.adapter.v2;

/**
 * Created with IntelliJ IDEA
 * Created By CQ
 * Date: 2020/8/22
 * Time: 20:04
 * 对象适配器
 * object Adapter
 */
public class AdapterTest2 {

    public static void main(String[] args) {
        Adaptee adaptee = new Adaptee();
        Adapter adapter = new Adapter(adaptee);
        adapter.outPut5v();
    }

}

class Adaptee {
    public int outPut220v() {
        return 220;
    }
}

interface Target {
    /**
     * 输出 5v 电;
     *
     * @return
     */
    int outPut5v();
}

class Adapter implements Target {
    private Adaptee adaptee;

    public Adapter(Adaptee adptee) {
        this.adaptee = adptee;
    }

    @Override
    public int outPut5v() {
        int i = adaptee.outPut220v();
        System.out.println(String.format("原始电压： %d v -> 输出电压 %d v", i, 5));
        return 5;
    }
}

