package com.echo.boot.leetcode.链表;

/**
 * Created with IntelliJ IDEA
 * Created By CQ
 * Date: 2020/5/2
 * Time: 19:12
 */
public class _237_删除链表中的节点 {
    public static void main(String[] args) {

        ListNode node1 = new ListNode(1);
        ListNode node2 = new ListNode(2);
        ListNode node3 = new ListNode(3);
        ListNode node4 = new ListNode(4);
        node1.next = node2;
        node2.next = node3;
        node3.next = node4;

        deleteNode(node2);
        System.out.println(node2);

    }

    public static void deleteNode(ListNode node) {
        node.val = node.next.val;
        node.next = node.next.next;
    }
}

