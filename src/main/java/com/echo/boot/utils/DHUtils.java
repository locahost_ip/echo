package com.echo.boot.utils;

import com.echo.boot.pojo.Person;

/**
 * Created with IntelliJ IDEA
 * Created By CQ
 * Date: 2020/3/27
 * Time: 11:33
 */
public class DHUtils {
    // DH 密钥交换法
    //DH算法通过数学定律保证了双方各自计算出的secretKey是相同的。
    // DH算法的本质就是双方各自生成自己的私钥和公钥，私钥仅对自己可见，然后交换公钥，并根据自己的私钥和对方的公钥，生成最终的密钥secretKey，
    //离散对数（英语：Discrete logarithm）是一种基于同余运算和原根的一种对数运算。

    public static void main(String[] args) {
        // Bob和Alice:
        Person bob = new Person("Bob");
        Person alice = new Person("Alice");

        // 各自生成KeyPair:
        bob.generateKeyPair();
        alice.generateKeyPair();

        // 双方交换各自的PublicKey:
        // Bob根据Alice的PublicKey生成自己的本地密钥:
        System.out.println(alice.publicKey);
        bob.generateSecretKey(alice.publicKey.getEncoded());
        // Alice根据Bob的PublicKey生成自己的本地密钥:
        alice.generateSecretKey(bob.publicKey.getEncoded());

        // 检查双方的本地密钥是否相同:
        bob.printKeys();
        alice.printKeys();
        // 双方的SecretKey相同，后续通信将使用SecretKey作为密钥进行AES加解密...
    }


}
