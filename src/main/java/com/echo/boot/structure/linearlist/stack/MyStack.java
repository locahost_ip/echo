package com.echo.boot.structure.linearlist.stack;

import com.echo.boot.structure.linearlist.array.MyArrayList;
import com.echo.boot.structure.linearlist.list.MyList;

/**
 * Created with IntelliJ IDEA
 * Created By CQ
 * Date: 2020/5/7
 * Time: 15:06
 * 后进先出
 * LIFO  last in first out
 *
 * @author CQ
 */
public class MyStack<E> {
    private MyList<E> list = new MyArrayList<>();

    public int size() {
        return list.size();
    }

    public boolean isEmpty() {
        return list.size() == 0;
    }

    public void push(E element) {
        list.add(element);
    }

    public E pop() {
        return list.remove(list.size() - 1);
    }

    public E top() {
        return list.get(list.size() - 1);
    }

    public void clear() {
        list.clear();
    }
}

