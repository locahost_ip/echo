package com.echo.boot.structure.set;

import com.echo.boot.structure.tree.binarytree.RedBlackTree;
import com.echo.boot.structure.tree.binarytree.Visitor;

import java.util.Comparator;

/**
 * Created with IntelliJ IDEA
 * Created By CQ
 * Date: 2020/6/2
 * Time: 19:29
 * 红黑树实现集合
 * 局限性就是 : 元素必须具有可比较性
 */
public class MyTreeSet<E> implements Set<E> {

    private RedBlackTree<E> tree;

    public MyTreeSet() {
        this(null);
    }

    public MyTreeSet(Comparator<E> comparator) {
        tree = new RedBlackTree<>(comparator);
    }

    @Override
    public int size() {
        return tree.size();
    }

    @Override
    public boolean isEmpty() {
        return tree.isEmpty();
    }


    @Override
    public void clear() {
        tree.clear();
    }

    @Override
    public boolean contains(E element) {
        return tree.contains(element);
    }

    @Override
    public void add(E element) {
        tree.add(element);
    }

    @Override
    public void remove(E element) {
        tree.remove(element);
    }

    @Override
    public void traversal(Visitor<E> visitor) {
        tree.inorderTraversal(visitor);
    }
}
