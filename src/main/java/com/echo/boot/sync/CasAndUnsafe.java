package com.echo.boot.sync;

import java.util.Arrays;
import java.util.concurrent.CountDownLatch;

/**
 * Created with IntelliJ IDEA
 * Created By CQ
 * Date: 2020/4/15
 * Time: 16:01
 */
public class CasAndUnsafe {
    private static int m = 0;

    public static void main(String[] args) throws InterruptedException {
        Thread[] threads = new Thread[100];
        CountDownLatch latch = new CountDownLatch(threads.length);
        for (int i = 0; i < threads.length; i++) {
            threads[i] = new Thread(() -> {
                synchronized (CasAndUnsafe.class) {
                    for (int j = 0; j < 100; j++) {
                        m++;
                    }
                }
                latch.countDown();
            });
        }

        Arrays.stream(threads).forEach((t) -> t.start());
        latch.await();
        System.out.println(m);
    }
}
