package com.echo.boot.structure.linearlist.queues.circle;

/**
 * Created with IntelliJ IDEA
 * Created By CQ
 * Date: 2020/5/8
 * Time: 20:24
 * 双端循环队列
 *
 * @author CQ
 */
public class MyCircleDeque<E> {
    private int front;
    private int size;
    private E[] elements;
    private static final int DEFAULT_CAPACITY = 10;

    public MyCircleDeque() {
        elements = (E[]) new Object[DEFAULT_CAPACITY];
    }

    public int size() {
        return size;
    }

    public boolean isEmpty() {
        return size == 0;
    }

    /**
     * @author CQ
     * @DESCRIPTION: 从尾部入队
     * @params: [element]
     * @return: void
     * @Date: 2020/6/2 14:40
     * @Modified By:
     */
    public void enQueueRear(E element) {
        // 头 1 r(2) null null null f(6) 7 8 9  尾
        ensureCapacity(size + 1);
        elements[index(size)] = element;
        size++;
    }

    /**
     * @author CQ
     * @DESCRIPTION: 从头部出队
     * @params: []
     * @return: E
     * @Date: 2020/6/2 14:42
     * @Modified By:
     */
    public E deQueueFront() {
        E frontElement = elements[front];
        elements[front] = null;
        front = index(1);
        size--;
        return frontElement;
    }

    /**
     * @author CQ
     * @DESCRIPTION: 从头部入队
     * @params: [element]
     * @return: void
     * @Date: 2020/6/2 14:43
     * @Modified By:
     */
    public void enQueueFront(E element) {
        ensureCapacity(size + 1);
        /*if(front - 1 < 0){
			front += elements.length;
		}
		front = front - 1;
		elements[front-1] = element;*/
        front = index(-1);
        elements[front] = element;
        size++;
    }

    /**
     * @author CQ
     * @DESCRIPTION: 从尾部出队
     * @params: []
     * @return: E
     * @Date: 2020/6/2 14:44
     * @Modified By:
     */
    public E deQueueRear() {
        int rearIndex = index(size - 1);
        E rear = elements[rearIndex];
        elements[rearIndex] = null;
        size--;
        return rear;
    }

    public E front() {
        return elements[front];
    }

    public E rear() {
        return elements[index(size - 1)];
    }

    private int index(int index) {

        // 为了循环
        // 通过当前索引获取真实的索引
        // 真实索引的优化 :  n % m
        index += front;
        if (index < 0) {
            return index + elements.length;
        }
        if (index < (elements.length << 1)) {
            return index - (index >= elements.length ? elements.length : 0);
        }
        return index % elements.length;
    }

    private void ensureCapacity(int capacity) {
        int oldCapacity = elements.length;
        if (oldCapacity >= capacity) {
            return;
        }
//        int newCapacity = oldCapacity << 2;
        // 1.5 倍
        int newCapacity = oldCapacity + (oldCapacity >> 1);
        E[] newElements = (E[]) new Object[newCapacity];
//        System.arraycopy(elements, 0, newElements, 0, elements.length);
        // 把原来的元素复制到新的数组中
        // 从front 的索引开始 移动 ; 怎么获取真实的索引:  （i + front） % element.length;
        for (int i = 0; i < size; i++) {
            newElements[i] = elements[index(i)];
        }
        elements = newElements;
        front = 0;
        System.out.println(capacity + "扩容为" + newCapacity + " size " + size);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("capacity=").append(elements.length);
        builder.append(", front=").append(front);
        builder.append(", size=").append(size);
        builder.append(", [");
        for (int i = 0; i < elements.length; i++) {
            if (i != 0) {
                builder.append(", ");
            }
            builder.append(elements[i]);
        }
        builder.append("]");
        return builder.toString();
    }


    public void clear() {
        for (int i = 0; i < size; i++) {
            elements[index(i)] = null;
        }
        size = 0;
        front = 0;
    }

}
