package com.echo.boot.structure.heap;

import java.util.Comparator;

/**
 * Created with IntelliJ IDEA
 * Created By CQ
 * Date: 2020/6/16
 * Time: 10:51
 *
 * @author Administrator
 */
public abstract class AbstracHeap<E> implements Heap<E> {
    protected int size;
    protected Comparator<E> comparator;

    public AbstracHeap() {
        this(null);
    }

    public AbstracHeap(Comparator<E> comparator) {
        this.comparator = comparator;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    protected int compare(E e1, E e2) {
        return comparator != null ? comparator.compare(e1, e2) : ((Comparable<E>) e1).compareTo(e2);
    }

}
