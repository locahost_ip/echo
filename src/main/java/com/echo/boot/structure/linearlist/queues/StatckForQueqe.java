package com.echo.boot.structure.linearlist.queues;

import java.util.Stack;

/**
 * Created with IntelliJ IDEA
 * Created By CQ
 * Date: 2020/5/7
 * Time: 22:29
 * 用栈实现队列
 * 准备两个栈 inStack,outStack;
 * 入队列
 * 入队时将所有元素入栈
 * 出对列
 * 如果outStack 为空的时候,将所有元素入 outStack 栈,弹出第一个元素
 * 如果outStack 不为空的时候,继续弹出元素
 * 因为队列依旧是从对头取出元素；
 *
 * @author CQ
 */
public class StatckForQueqe<E> {
    private Stack<E> inStack = new Stack<>();
    private Stack<E> outStack = new Stack<>();


    public int size() {
        return inStack.size();
    }

    public boolean isEmpty() {
        return outStack.isEmpty() && inStack.isEmpty();
    }

    public void enQueue(E element) {
        inStack.push(element);
    }

    public E deQueue() {
        checkOutStack();
        return outStack.pop();
    }

    public E front() {
        checkOutStack();
        return outStack.pop();
    }

    public void clear() {
        outStack.clear();
        inStack.clear();
    }

    private void checkOutStack() {
        if (outStack.isEmpty()) {
            for (int i = 0; i < inStack.size(); i++) {
                outStack.push(inStack.pop());
            }
        }
    }


}
