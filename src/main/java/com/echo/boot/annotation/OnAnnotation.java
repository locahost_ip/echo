package com.echo.boot.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

/**
 * Created with IntelliJ IDEA
 * Created By CQ
 * Date: 2020/7/3
 * Time: 20:52
 *
 * @author Administrator
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.ANNOTATION_TYPE})
public @interface OnAnnotation {
    @OnAnnotation(name = "Annotation....on")
    String name() default "";
}
