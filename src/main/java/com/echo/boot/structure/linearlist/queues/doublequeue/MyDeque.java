package com.echo.boot.structure.linearlist.queues.doublequeue;

import com.echo.boot.structure.linearlist.linked.doublelinkedlist.MyLinkedList;
import com.echo.boot.structure.linearlist.list.MyList;

/**
 * Created with IntelliJ IDEA
 * Created By CQ
 * Date: 2020/5/8
 * Time: 8:19
 * 双端队列
 * 可以在头尾两端添加元素
 *
 * @author CQ
 */
public class MyDeque<E> {
    private MyList<E> list = new MyLinkedList<>();

    public int size() {
        return list.size();
    }

    public boolean isEmpty() {
        return list.isEmpty();
    }

    /**
     * @author CQ
     * @DESCRIPTION: 从尾部入队
     * @params: [element]
     * @return: void
     * @Date: 2020/6/2 11:18
     * @Modified By: CQ
     */
    public void enQueueRear(E element) {
        list.add(element);
    }


    /**
     * @author CQ
     * @DESCRIPTION: 从头部入队
     * @params: []
     * @return: E
     * @Date: 2020/6/2 11:22
     * @Modified By:
     */
    public E deQueueFront() {
        return list.remove(0);
    }

    /**
     * @author CQ
     * @DESCRIPTION: 从头部入队
     * @params: [element]
     * @return: void
     * @Date: 2020/6/2 11:23
     * @Modified By:
     */
    public void enQueueFront(E element) {
        list.add(0, element);
    }

    /**
     * @author CQ
     * @DESCRIPTION: 从尾部出队
     * @params: []
     * @return: E
     * @Date: 2020/6/2 11:21
     * @Modified By:
     */
    public E deQueueRear() {
        return list.remove(size() - 1);
    }

    public E front() {
        return list.remove(0);
    }

    public E rear() {
        return list.remove(size() - 1);
    }
}

