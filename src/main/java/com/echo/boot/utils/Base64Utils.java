package com.echo.boot.utils;

import java.util.Arrays;
import java.util.Base64;

/**
 * Created with IntelliJ IDEA
 * Created By CQ
 * Date: 2020/3/26
 * Time: 10:06
 * 如果是 负数的 十进制 位 用 补码计算 16进制位
 * Base64编码可以把任意长度的二进制数据变为纯文本，且只包含A~Z、a~z、0~9、+、/、=这些字符。它的原理是把3字节的二进制数据按6bit一组，用4个int整数表示，然后查表，把int整数用索引对应到字符，得到编码后的字符串。
 * 1byte = 2个16进制位 = 4 个 8进制位
 * 1byte = 8bit
 * 1bit = 2个进制位
 * <p>
 * 1byte = 2^3 bit
 * 1KB = 1024 byte = 2^10 byte
 * 1MB = 1024 KB = 2^20 byte
 * 1GB = 1024 MB = 2^30 byte
 * <p>
 * ┌───────────────┬───────────────┬───────────────┐
 * │      e4       │      b8       │      ad       │
 * └───────────────┴───────────────┴───────────────┘
 * ┌─┬─┬─┬─┬─┬─┬─┬─┬─┬─┬─┬─┬─┬─┬─┬─┬─┬─┬─┬─┬─┬─┬─┬─┐
 * │1│1│1│0│0│1│0│0│1│0│1│1│1│0│0│0│1│0│1│0│1│1│0│1│
 * └─┴─┴─┴─┴─┴─┴─┴─┴─┴─┴─┴─┴─┴─┴─┴─┴─┴─┴─┴─┴─┴─┴─┴─┘
 * ┌───────────┬───────────┬───────────┬───────────┐
 * │    39     │    0b     │    22     │    2d     │
 * └───────────┴───────────┴───────────┴───────────┘
 */
public class Base64Utils {
    public static void main(String[] args) {

        /**
         如果输入的byte[]数组长度不是3的整数倍肿么办？这种情况下，需要对输入的末尾补一个或两个0x00，编码后，在结尾加一个=表示补充了1个0x00，加两个=表示补充了2个0x00，解码的时候，去掉末尾补充的一个或两个0x00即可。
         实际上，因为编码后的长度加上=总是4的倍数，所以即使不加=也可以计算出原始输入的byte[]。Base64编码的时候可以用withoutPadding()去掉=，解码出来的结果是一样的：
         */

        byte[] input = new byte[]{(byte) 0xe4, (byte) 0xb8, (byte) 0xad, 0x21};
        String b64encoded = Base64.getEncoder().encodeToString(input);
        // 不填充 withoutPadding();
        String b64encoded2 = Base64.getEncoder().withoutPadding().encodeToString(input);
        System.out.println(b64encoded);
        System.out.println(b64encoded2);
        byte[] output = Base64.getDecoder().decode(b64encoded2);
        System.out.println(Arrays.toString(output));
    }

    public static String encode(byte[] encryptedData) {
        return Base64.getEncoder().encodeToString(encryptedData);
    }

    public static byte[] decode(String dencryptData) {
        return Base64.getDecoder().decode(dencryptData);
    }


}
