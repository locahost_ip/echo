package com.echo.boot.enumeration;

/**
 * Created with IntelliJ IDEA
 * Created By CQ
 * Date: 2020/7/3
 * Time: 20:00
 */
public enum Operation {


    /**
     * 执行加法
     */
    PLUS("加法") {
        @Override
        public double calculate(double x, double y) {
            return x + y;
        }
    },

    /**
     * 用于执行减法运算
     */
    MINUS("减法") {
        @Override
        public double calculate(double x, double y) {
            return x - y;
        }

    },

    /**
     * 用于执行乘法运算
     */
    TIMES("乘法") {
        @Override
        public double calculate(double x, double y) {
            return x * y;
        }

    },

    /**
     * 用于执行除法运算
     */
    DIVIDE("除法") {
        @Override
        public double calculate(double x, double y) {
            return x / y;
        }

    };

    private String name;

    Operation(String name) {
        this.name = name;
    }


    /**
     * @author CQ
     * @DESCRIPTION: 为该枚举类定义一个抽象方法，枚举类中所有的枚举值都必须实现这个方法
     * @params: [x, y]
     * @return: double
     * @Date: 2020/7/3 20:31
     * @Modified By:
     */
    public abstract double calculate(double x, double y);


    public String getName() {
        return this.name;
    }

}
