package com.echo.boot.functions;

import java.util.function.Predicate;
import java.util.function.Supplier;

/**
 * Created with IntelliJ IDEA
 * Created By CQ
 * Date: 2020/9/7
 * Time: 17:27
 */
public class SupplierTest {
    public static void main(String[] args) {
        String s1 = "Jack";
        String s2 = "Rose";
        //getFirstNotEmptyString 返回传入的第一个不为空的字符串，但是有时候会浪费性能，比如第二个字符串是由某个函数构造的，虽然第一个字符串已经确定不为空，但依旧执行了构造第二个字符串的函数。
//        System.out.println(getFirstNotEmptyString(s1, makeString()));
        // 使用了函数式接口, 不会执行 makeString()
        System.out.println(getFirstNotEmptyString(s1, () -> makeString()));
    }

    static String makeString() {
        System.out.println("makeString");
        return String.format("%d %d %d", 1, 2, 3);
    }

    //获取第一个不为空的字符串, 正常写法
    static String getFirstNotEmptyString(String s1, String s2) {
        if (s1 != null || s1.length() > 0) return s1;
        if (s2 != null || s2.length() > 0) return s2;
        return null;
    }

    // 获取第一个不为空的字符串, 函数式接口
    static String getFirstNotEmptyString(String s1, Supplier<String> supplier) {
        if (s1 != null || s1.length() > 0) return s1;
        String s2 = supplier.get();
        if (s2 != null || s2.length() > 0) return s2;
        return null;
    }
}
