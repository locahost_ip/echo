package com.echo.boot.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created with IntelliJ IDEA
 * Created By CQ
 * Date: 2020/1/21
 * Time: 21:03
 *
 * @author Administrator
 * <p>
 * 全局捕获异常
 */
@RestController
public class ErrorController {
    @RequestMapping("errorHandler")
    public String errorHandler() {
        int i = 5 / 0;
        return "success:" + i;
    }
}
