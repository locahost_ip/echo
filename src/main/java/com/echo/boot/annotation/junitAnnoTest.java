package com.echo.boot.annotation;

public class junitAnnoTest {


    @MyBefore
    public void before() {
        System.out.println("初始化");
    }

    @MyTest
    public void testSave() {
        System.out.println("保存");
    }

    @MyTest
    public void testDelete() {
        System.out.println("删除");
    }

    @MyAfter
    public void destory() {
        System.out.println("销毁");
    }
}

