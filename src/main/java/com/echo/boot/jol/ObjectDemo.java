package com.echo.boot.jol;

import org.openjdk.jol.info.ClassLayout;

import java.util.concurrent.TimeUnit;

/**
 * Created with IntelliJ IDEA
 * Created By CQ
 * Date: 2020/4/16
 * Time: 13:28
 */
public class ObjectDemo {
    public static void main(String[] args) throws Exception {
//        TimeUnit.SECONDS.sleep(5L);
        /// 偏向锁 101  必须在4秒后开启
        Object o = new Object();
        System.out.println(ClassLayout.parseInstance(o).toPrintable());

//        System.out.println("============================================");
        // 开启轻量级锁
//        synchronized(o){
//            System.out.println(ClassLayout.parseInstance(o).toPrintable());
//        }
    }
}
