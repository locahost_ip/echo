package com.echo.boot.sync.waitandnotify;

/**
 * Created with IntelliJ IDEA
 * Created By CQ
 * Date: 2020/9/13
 * Time: 21:59
 */
public class Consumer implements Runnable {

    private Drop drop;

    public Consumer(Drop drop) {
        this.drop = drop;
    }

    @Override
    public void run() {
        String food = null;

        while ((food = drop.get()) != null) {
            System.out.format("消费者接收到生产者生产的食物：%s%n", food);
            try {
                Thread.sleep(1000); // 消费者吃食物2秒
            } catch (InterruptedException e) {
            }
        }
    }
}
