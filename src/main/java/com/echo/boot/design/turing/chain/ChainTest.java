package com.echo.boot.design.turing.chain;

/**
 * Created with IntelliJ IDEA
 * Created By CQ
 * Date: 2020/8/29
 * Time: 23:34
 * 责任链
 * 为请求创建一个接受者对象的链
 */
public class ChainTest {
    public static void main(String[] args) {
        MyRequest request = new MyRequest.MyRequestBuilder().frequentOk(false).logedOn(true).build();
        RequestFrequentFHander requestFrequentFHander = new RequestFrequentFHander(new LoginHander(null));
        if (requestFrequentFHander.process(request)) {
            System.out.println("正常业务处理");
        } else {
            System.out.println("访问异常");
        }
    }

}


class MyRequest {
    private boolean loggedOn;
    private boolean frequentOk;
    private boolean isPermits;
    private boolean containsSensitiveWords;
    private String requestBody;

    private MyRequest(boolean loggedOn, boolean frequentOk, boolean isPermits, boolean containsSensitiveWords, String requestBody) {
        this.loggedOn = loggedOn;
        this.frequentOk = frequentOk;
        this.isPermits = isPermits;
        this.containsSensitiveWords = containsSensitiveWords;
        this.requestBody = requestBody;
    }


    public boolean isLoggedOn() {
        return loggedOn;
    }

    public boolean isFrequentOk() {
        return frequentOk;
    }

    public boolean isPermits() {
        return isPermits;
    }

    public boolean isContainsSensitiveWords() {
        return containsSensitiveWords;
    }

    static class MyRequestBuilder {
        private boolean loggedOn;
        private boolean frequentOk;
        private boolean isPermits;
        private boolean containsSensitiveWords;
        private String requestBody;


        public MyRequestBuilder logedOn(boolean loggedOn) {
            this.loggedOn = loggedOn;
            return this;
        }

        public MyRequestBuilder frequentOk(boolean frequentOk) {
            this.frequentOk = frequentOk;
            return this;
        }

        public MyRequestBuilder isPermits(boolean isPermits) {
            this.isPermits = isPermits;
            return this;
        }

        public MyRequestBuilder containsSensitiveWords(boolean containsSensitiveWords) {
            this.containsSensitiveWords = containsSensitiveWords;
            return this;
        }

        public MyRequest build() {
            return new MyRequest(this.loggedOn, this.frequentOk, this.isPermits, this.containsSensitiveWords, this.requestBody);
        }
    }
}

abstract class Hander {
    private Hander next;

    public Hander(Hander next) {
        this.next = next;
    }

    public Hander getNext() {
        return next;
    }

    public void setNext(Hander next) {
        this.next = next;
    }

    // 只有处理完返回true 时,才继续执行下一个责任连
    abstract boolean process(MyRequest request);
}


class RequestFrequentFHander extends Hander {
    public RequestFrequentFHander(Hander next) {
        super(next);
    }

    @Override
    boolean process(MyRequest request) {
        if (request.isFrequentOk()) {
            System.out.println("访问频率口控制。。。");
            Hander next = getNext();
            if (next == null) {
                return true;
            }
            return next.process(request);
        }
        return false;
    }
}

class LoginHander extends Hander {

    public LoginHander(Hander next) {
        super(next);
    }

    @Override
    boolean process(MyRequest request) {
        if (request.isLoggedOn()) {
            System.out.println("登陆验证。。。。。");
            Hander next = getNext();
            if (next == null) {
                return true;
            }
            return next.process(request);
        }
        return false;
    }
}