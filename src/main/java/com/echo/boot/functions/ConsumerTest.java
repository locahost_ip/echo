package com.echo.boot.functions;

import java.util.function.Consumer;

/**
 * Created with IntelliJ IDEA
 * Created By CQ
 * Date: 2020/9/7
 * Time: 17:48
 */
public class ConsumerTest {
    public static void main(String[] args) {
        int[] nums = {11, 33, 44, 88, 77, 66};
        forEach(nums, (n) -> {
            String result = ((n & 1) == 0) ? "偶数" : "奇数";
            System.out.println(n + "是" + result);
        });

        forEach(nums, (n) -> {
            String result = ((n & 1) == 0) ? "偶数" : "奇数";
            System.out.println(n + "是" + result);
        }, (n) -> {
            String result = ((n % 3) == 0) ? "能" : "不能";
            System.out.println(n + result + "被3整除");
        });
    }


    static void forEach(int[] nums, Consumer<Integer> c) {
        if (nums == null || c == null) {
            return;
        }
        for (int n : nums) {
            c.accept(n);
        }
    }

    static void forEach(int[] nums, Consumer<Integer> c1, Consumer<Integer> c2) {
        if (nums == null || c1 == null || c2 == null) {
            return;
        }
        for (int n : nums) {
            c1.andThen(c2).accept(n);
        }
    }


}