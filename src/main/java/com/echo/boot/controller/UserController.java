package com.echo.boot.controller;

import com.echo.boot.pojo.User;
import com.echo.boot.result.BaseResult;
import com.echo.boot.service.UserService;
import com.google.common.util.concurrent.RateLimiter;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created with IntelliJ IDEA
 * Created By CQ
 * Date: 2020/7/4
 * Time: 15:20
 *
 * @author Administrator
 */
@RestController
@RequestMapping("user")
// Swagger @Api
@Api(value = "用户控制器", tags = "用户控制器")
public class UserController {

    @Autowired
    private UserService userService;

    RateLimiter rateLimiter = RateLimiter.create(1.0);


    /**
     * 接受 pojo 对象
     *
     * @param user
     */
    @ApiOperation(value = "添加用户", notes = "添加用户")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "username", value = "用户名", required = true, dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "age", value = "年龄", required = true, dataType = "Integer", paramType = "query"),
            @ApiImplicitParam(name = "password", value = "密码", required = true, dataType = "String", paramType = "query")
    })
    @RequestMapping(value = "insert", method = {RequestMethod.GET, RequestMethod.POST})
    public BaseResult<User> insert(User user) {
        int insert = userService.insert(user);
        return BaseResult.successWithData(user);
    }

    @ApiOperation(value = "用户列表", notes = "用户列表")
    @RequestMapping(value = "userList", method = {RequestMethod.POST})
    public List<User> selectAllUser() {
        return userService.selectAllUser();
    }


    @ApiOperation(value = "获取用户详细信息", notes = "根据url的id来获取用户详细信息")
    @ApiImplicitParam(name = "id", value = "用户ID", required = true, dataType = "Long", paramType = "path")
    @RequestMapping(value = "info/{id}", method = {RequestMethod.GET})
    public BaseResult<User> userInfo(@RequestParam(value = "id", required = false, defaultValue = "1") Integer id) {
        //  // @Pathvariable 注解绑定它传过来的值到方法的参数上
        User user = userService.selectByPrimaryKey(id);
        return BaseResult.successWithData(user);
    }

    @ApiOperation(value = "编辑用户信息", notes = "根据url的id来获取用户信息并并编辑")
    @ApiImplicitParam(name = "id", value = "用户ID", required = true, dataType = "Long", paramType = "path")
    @RequestMapping(value = "edit/{id}", method = {RequestMethod.POST})
    public BaseResult<User> updateUser(Integer id, User user) {
        userService.updateByPrimaryKey(user);
        return BaseResult.success();
    }


}
