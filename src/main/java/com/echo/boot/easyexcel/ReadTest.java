package com.echo.boot.easyexcel;

import com.alibaba.excel.EasyExcel;

import javax.swing.filechooser.FileSystemView;
import java.math.BigDecimal;

public class ReadTest {


    public static void main(String[] args) {
        FileSystemView fsv = FileSystemView.getFileSystemView();
        String desktop = fsv.getHomeDirectory().getPath();
        String filePath = desktop + "/user_base.xlsx";
        EasyExcel.read(filePath, new ExcelListener()).sheet().doRead();


    }


  
}
