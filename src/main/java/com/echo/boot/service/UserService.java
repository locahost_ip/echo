package com.echo.boot.service;

import com.echo.boot.pojo.User;

import java.util.List;

/**
 * Created with IntelliJ IDEA
 * Created By CQ
 * Date: 2020/7/4
 * Time: 15:55
 */
public interface UserService {
    int deleteByPrimaryKey(Integer id);

    int insert(User record);

    int insertSelective(User record);

    User selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(User record);

    int updateByPrimaryKey(User record);

    List<User> selectUserByName(String name);

    List<User> selectAllUser();

    List<User> selectUserByCondition(User user);

}
