package com.echo.boot.design.turing.template;

/**
 * Created with IntelliJ IDEA
 * Created By CQ
 * Date: 2020/8/29
 * Time: 21:25
 * 模版方法
 * 定义一个操作的算法骨架，而将一些步骤延迟到子类中，
 * Template Method 可以使得子类不改变算法的结构即可重新定义改算法的某些特定的步骤
 */
public class Template {
    public static void main(String[] args) {
        AbstractClass abstractClass = new subClass();
        abstractClass.operation();

    }
}

abstract class AbstractClass {

    public void operation() {
        System.out.println("pre");
        System.out.println("step 1");
        System.out.println("step 2");
        System.out.println("step 3");
        templateMethod();
        System.out.println("end ");


    }

    abstract protected void templateMethod();
}

class subClass extends AbstractClass {

    @Override
    protected void templateMethod() {
        System.out.println("sub class executed..");
    }
}
