package com.echo.boot.service.imp;

import com.echo.boot.mapper.ThemeMapper;
import com.echo.boot.pojo.Theme;
import com.echo.boot.service.ThemeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created with IntelliJ IDEA
 * Created By CQ
 * Date: 2020/7/1
 * Time: 21:38
 *
 * @author Administrator
 */
@Service("ThemeService")
public class ThemeServiceImp implements ThemeService {

    @Autowired
    private ThemeMapper themeMapper;

    @Override
    public int deleteByPrimaryKey(Integer id) {
        return themeMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int insert(Theme record) {
        return themeMapper.insert(record);
    }

    @Override
    public int insertSelective(Theme record) {
        return themeMapper.insertSelective(record);
    }

    @Override
    public Theme selectByPrimaryKey(Integer id) {
        return themeMapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKeySelective(Theme record) {
        return themeMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(Theme record) {
        return themeMapper.updateByPrimaryKey(record);
    }
}
