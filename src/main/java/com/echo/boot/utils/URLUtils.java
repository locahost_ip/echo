package com.echo.boot.utils;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

/**
 * Created with IntelliJ IDEA
 * Created By CQ
 * Date: 2020/3/27
 * Time: 12:16
 * <p>
 * URL编码，是因为出于兼容性考虑，很多服务器只识别ASCII字符。但如果URL中包含中文、日文这些非ASCII字符怎么办？不要紧，URL编码有一套规则：
 * <p>
 * 如果字符是A~Z，a~z，0~9以及-、_、.、*，则保持不变；
 * 如果是其他字符，先转换为UTF-8编码，然后对每个字节以%XX表示。
 * <p>
 * 字符中的UTF-8编码是0xe4b8ad，因此，它的URL编码是%E4%B8%AD。URL编码总是大写。
 */
public class URLUtils {
    public static void main(String[] args) throws UnsupportedEncodingException {
        String encoded = URLEncoder.encode("中文!", StandardCharsets.UTF_8.name());
        System.out.println(encoded);
    }
}
