package com.echo.boot.controller;

import com.echo.boot.pojo.Theme;
import com.echo.boot.service.ThemeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.UUID;

/**
 * Created with IntelliJ IDEA
 * Created By CQ
 * Date: 2020/7/1
 * Time: 21:37
 */
@RestController
@RequestMapping("theme")
public class ThemeController {

    @Autowired
    private ThemeService themeServie;

    @RequestMapping("insertTheme")
    public int insertTheme(Theme record) {
        if (record == null) {
            Theme theme = new Theme();
            theme.setName(UUID.randomUUID().toString().replace("-", ""));
            theme.setCreateDate(LocalDateTime.now());
            theme.setDelFlag(1);
        }
        return themeServie.insert(record);
    }

    @RequestMapping("selectTheme")
    public Theme selectByPrimaryKey(Integer id) {
        return themeServie.selectByPrimaryKey(id);
    }
}
