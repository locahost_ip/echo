package com.echo.boot.threadpools;

import java.util.concurrent.Callable;

/**
 * Created with IntelliJ IDEA
 * Created By CQ
 * Date: 2020/11/5
 * Time: 17:41
 */
public class MyCallable implements Callable<Integer> {
    @Override
    public Integer call() throws Exception {
        return 1;
    }
}
