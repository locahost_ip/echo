package com.echo.boot.service;

import com.echo.boot.pojo.Theme;

/**
 * Created with IntelliJ IDEA
 * Created By CQ
 * Date: 2020/7/1
 * Time: 21:37
 */
public interface ThemeService {
    int deleteByPrimaryKey(Integer id);

    int insert(Theme record);

    int insertSelective(Theme record);

    Theme selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Theme record);

    int updateByPrimaryKey(Theme record);
}
