package com.echo.boot.structure.linearlist;

import com.echo.boot.structure.linearlist.array.MyArrayList;
import com.echo.boot.structure.linearlist.linked.circle.MyCircleLinkedList;
import com.echo.boot.structure.linearlist.linked.circle.MySingleCircleLinkedList;
import com.echo.boot.structure.linearlist.linked.doublelinkedlist.MyLinkedList;
import com.echo.boot.structure.linearlist.queues.circle.MyCircleDeque;
import com.echo.boot.structure.linearlist.queues.circle.MyCircleQueue;
import com.echo.boot.structure.linearlist.queues.doublequeue.MyDeque;
import com.echo.boot.structure.linearlist.queues.queue.MyQueue;
import com.echo.boot.structure.linearlist.stack.MyStack;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created with IntelliJ IDEA
 * Created By CQ
 * Date: 2020/4/29
 * Time: 17:59
 *
 * @author CQ
 */
public class Main {
    public static void main(String[] args) {
        ArrayList<String> list = new ArrayList<>();
//        MySingleCircleLinkedListTest();
//        MyCircleLinkedListTest();
//        josephus();
//        MyStackTest();
//        stackTest();
//        MyQueueTest();
//        MyDequeTest();
//        MyCicleQueueTest();
//        MyCicleDequeTest();
//        String str =  "-44.3";
//        double v = Double.parseDouble(str);
//        System.out.println(v >0);
//        System.out.println(v);



    }

    static void MyArrayListTest() {
        // 动态数组缩容
        MyArrayList<Integer> list = new MyArrayList<>();
        for (int i = 0; i < 50; i++) {
            list.add(i);
        }

        for (int i = 0; i < 50; i++) {
            list.remove(0);
        }
        System.out.println(list);
    }

    static void MyLinkedListTest() {
        MyLinkedList<Integer> list = new MyLinkedList<>();
        list.add(10);
        list.add(11);
        list.add(12);
        list.add(13);
        list.add(14);
        list.add(15);
        list.add(0, 19);
        list.add(list.size(), 25);
        list.remove(0);
        System.out.println(list.size());
        System.out.println(list);
    }

    static void MySingleCircleLinkedListTest() {
        MySingleCircleLinkedList<Integer> list = new MySingleCircleLinkedList<>();
        list.add(10);
        list.add(11);
        list.add(12);
        list.add(13);
        list.add(14);
        list.add(15);
        list.add(0, 19);
        list.add(list.size(), 25);
        list.remove(0);
        list.remove(list.size() - 1);
        System.out.println(list.size());
        System.out.println(list);

    }

    static void MyCircleLinkedListTest() {
        MyCircleLinkedList<Integer> list = new MyCircleLinkedList<>();
        list.add(10);
        list.add(11);
        list.add(12);
        list.add(13);
        list.add(14);
        list.add(15);
        list.add(0, 19);
        list.add(list.size(), 25);
        list.remove(0);
        list.remove(list.size() - 1);
        System.out.println(list.size());
        System.out.println(list);
    }

    static void josephus() {
        // 约瑟夫问题
        MyCircleLinkedList<Integer> list = new MyCircleLinkedList<>();
        for (int i = 1; i <= 100; i++) {
            list.add(i);
        }

        // 让指针指向第一个元素
        list.reset();
        // n 数到第几个数
        while (!list.isEmpty()) {
            int n = 3;
            while ((--n) > 0) {
                list.next();
            }
            System.out.println(list.remove());
        }
    }

    static void MyStackTest() {
        MyStack<Integer> stack = new MyStack<>();
        stack.push(1);
        stack.push(2);
        stack.push(3);
        stack.push(4);
        stack.push(5);
        stack.push(6);
        while (!stack.isEmpty()) {
            System.out.println(stack.pop());
        }
    }

    static void stackTest() {
        MyStack<String> stack1 = new MyStack<>();
        MyStack<String> stack2 = new MyStack<>();

        stack1.push("taobao");
        stack1.push("jingdong");
        stack1.push("meituan");

        // back 后退
        stack2.push(stack1.pop());
        System.out.println(stack1.top());

        // forward 前进
        stack1.push(stack2.pop());
        System.out.println(stack1.top());

        // back; 后退
        stack2.push(stack1.pop());
        System.out.println(stack1.top());

        // back 后退
        stack2.push(stack1.pop());
        System.out.println(stack1.top());

        stack1.push("huawei");
        stack2.clear();
    }

    static void MyQueueTest() {
        MyQueue<Integer> queue = new MyQueue<>();
        queue.enQueue(10);
        queue.enQueue(20);
        queue.enQueue(30);
        queue.enQueue(40);
        queue.enQueue(50);
        queue.enQueue(60);
//        System.out.println(queue.size());
        int size = queue.size();
        for (int i = 0; i < size; i++) {
            System.out.println(queue.deQueue());
        }
    }

    static void MyDequeTest() {
        MyDeque<Integer> deque = new MyDeque<>();
        deque.enQueueFront(11);
        deque.enQueueFront(22);
        deque.enQueueRear(33);
        deque.enQueueRear(44);
        deque.enQueueFront(99);
        /* 尾  44  33   11  22  99 头**/
//        while (!deque.isEmpty()) {
//            System.out.println(deque.deQueueFront());
//        }

        while (!deque.isEmpty()) {
            System.out.println(deque.deQueueRear());
        }
    }

    static void MyCicleQueueTest() {
        MyCircleQueue<Integer> queue = new MyCircleQueue<>();
        for (int i = 0; i < 10; i++) {
            queue.enQueue(i);
        }
        for (int i = 0; i < 5; i++) {
            queue.deQueue();
        }
        for (int i = 15; i < 29; i++) {
            queue.enQueue(i);
        }
        System.out.println(queue);
        while (!queue.isEmpty()) {
            System.out.println(queue.deQueue());
        }
    }

    static void MyCicleDequeTest() {
        MyCircleDeque<Integer> deque = new MyCircleDeque<>();
        //  头 5,4,3,2,1,100,101,102,103,104尾//
        // 扩容 1.5 倍  10 + 5
        //  头 5,4,3,2,1,100,101,102,103,104,105,106,8,7,6 尾//
        // 扩容 15 + 7 = 22
        //  头 8,7,6,5,4,3,2,1,100,101,102,103,104,105,106,107,108,109, null, null,10,9 尾//
        for (int i = 0; i < 10; i++) {
            deque.enQueueFront(i + 1);
            deque.enQueueRear(i + 100);
        }
        System.out.println(deque);
        //  头 8, 7, 6, 5, 4, 3, 2, 1, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, null, null, 10,9 尾//
        // 第一次
        //  头 8, 7, 6, 5, 4, 3, 2, 1, 100, 101, 102, 103, 104, 105, 106, 107, 108, null, null, null, null,9 尾//
        // 第二次
        //  头 8, 7, 6, 5, 4, 3, 2, 1, 100, 101, 102, 103, 104, 105, 106, 107, null, null, null, null, null,null 尾//
        // 第三次
        //  头 null, 7, 6, 5, 4, 3, 2, 1, 100, 101, 102, 103, 104, 105, 106, null, null, null, null, null, null,null 尾//
        for (int i = 0; i < 3; i++) {
            deque.deQueueFront();
            deque.deQueueRear();
            System.out.println(deque);
        }
        //  头 11, 7, 6, 5, 4, 3, 2, 1, 100, 101, 102, 103, 104, 105, 106, null, null, null, null, null, null,null 尾//
        deque.enQueueFront(11);
        //  头 11, 7, 6, 5, 4, 3, 2, 1, 100, 101, 102, 103, 104, 105, 106, null, null, null, null, null, null,12 尾//
        deque.enQueueFront(12);
        System.out.println(deque);

        deque.clear();
//        while (!deque.isEmpty()) {
//            System.out.println(deque.deQueueFront());
//        }
    }
}
