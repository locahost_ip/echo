package com.echo.boot.utils;

import com.echo.boot.pojo.RasPojo;

import java.math.BigInteger;

/**
 * Created with IntelliJ IDEA
 * Created By CQ
 * Date: 2020/3/27
 * Time: 12:13
 * <p>
 * 在实际应用的时候，非对称加密总是和对称加密一起使用
 * 假设小明需要给小红需要传输加密文件，他俩首先交换了各自的公钥，然后：
 * <p>
 * 小明生成一个随机的AES口令，然后用小红的公钥通过RSA加密这个口令，并发给小红；
 * 小红用自己的RSA私钥解密得到AES口令；
 * 双方使用这个共享的AES口令用AES加密通信
 */
public class RSAUtils {

    public static void main(String[] args) throws Exception {
        // 明文:
        byte[] plain = "Hello, encrypt use RSA".getBytes("UTF-8");
        // 创建公钥／私钥对:
        RasPojo alice = new RasPojo("Alice");
        // 用Alice的公钥加密:
        byte[] pk = alice.getPublicKey();
        System.out.println(String.format("public key: %x", new BigInteger(1, pk)));
        byte[] encrypted = alice.encrypt(plain);
        System.out.println(String.format("encrypted: %x", new BigInteger(1, encrypted)));
        // 用Alice的私钥解密:
        byte[] sk = alice.getPrivateKey();
        System.out.println(String.format("private key: %x", new BigInteger(1, sk)));
        byte[] decrypted = alice.decrypt(encrypted);
        System.out.println(new String(decrypted, "UTF-8"));
    }
}
