package com.echo.boot.sync;

import java.util.concurrent.TimeUnit;

/**
 * Created with IntelliJ IDEA
 * Created By CQ
 * Date: 2020/4/16
 * Time: 14:23
 */
public class T01_HelloVolatile {
    boolean running = true;

    void m() {
        System.out.println("m start");
        while (running) {

        }
        System.out.println("m end");
    }

    public static void main(String[] args) {
        T01_HelloVolatile t = new T01_HelloVolatile();
        new Thread(t::m, "t1");
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        t.running = true;
    }
}
