package com.echo.boot.advance;

import java.text.DecimalFormat;

/**
 * Created with IntelliJ IDEA
 * Created By CQ
 * Date: 2020/9/11
 * Time: 17:15
 */
public class DecimalFormatTest {
    public static void main(String[] args) {
        customFormat("###,###.###", 123456.789); // 123,456.789
        customFormat("###.###", 123456.789);     // 123456.789
        customFormat("000000.000", 123.78);         // 000123.780
        customFormat("$###,###.###", 12345.67);     // $12,345.67
    }

    static void customFormat(String pattern, double value) {
        DecimalFormat decimalFormat = new DecimalFormat(pattern);
        String format = decimalFormat.format(value);
        System.out.println(format);
    }
}
