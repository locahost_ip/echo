package com.echo.boot.transaction;

import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.interceptor.DefaultTransactionAttribute;

/**
 * Created with IntelliJ IDEA
 * Created By CQ
 * Date: 2020/7/3
 * Time: 9:36
 *
 * @author Administrator
 */
public class TransactonUtils {
    public static void main(String[] args) {
        DataSourceTransactionManager dataSourceTransactionManager = new DataSourceTransactionManager();
        TransactionStatus transaction = dataSourceTransactionManager.getTransaction(new DefaultTransactionAttribute());
    }
}
