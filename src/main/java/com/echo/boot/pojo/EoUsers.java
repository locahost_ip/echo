package com.echo.boot.pojo;

import java.util.Date;

 /**
 *Created with IntelliJ IDEA
 *Created By CQ
 *Date: 2020/11/4
 *Time: 10:33 
 */
public class EoUsers {
    private Object id;

    private String customercode;

    private String customername;

    private String shortname;

    private String loginname;

    private String password;

    private String name;

    private String telephone;

    private String address;

    private String taxno;

    private String bankname;

    private String bankaccountno;

    /**
    * 1,普通发票；2，电子发票，3，增值税普票, 4，增值税专票
    */
    private Integer invoicetype;

    private String merpubkey;

    private String gdsypubkey;

    private String gdsyprikey;

    private String deskey;

    private Integer status;

    private Date createdate;

    private Integer businessmode;

    private String oucode;

    private String accountowner;

    private Integer isinner;

    /**
    * 是否返还（0：不返还；1：返还）
    */
    private Integer ispref;

    private String createby;

    private String contracturl;

    private String validdays;

    private String jmcode;

    private Integer invoicedate;

    private String invoiceeffectdate;

    private String invoicecontactname;

    private String invoicecontacttel;

    private String invoicecontactaddress;

    public Object getId() {
        return id;
    }

    public void setId(Object id) {
        this.id = id;
    }

    public String getCustomercode() {
        return customercode;
    }

    public void setCustomercode(String customercode) {
        this.customercode = customercode;
    }

    public String getCustomername() {
        return customername;
    }

    public void setCustomername(String customername) {
        this.customername = customername;
    }

    public String getShortname() {
        return shortname;
    }

    public void setShortname(String shortname) {
        this.shortname = shortname;
    }

    public String getLoginname() {
        return loginname;
    }

    public void setLoginname(String loginname) {
        this.loginname = loginname;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTaxno() {
        return taxno;
    }

    public void setTaxno(String taxno) {
        this.taxno = taxno;
    }

    public String getBankname() {
        return bankname;
    }

    public void setBankname(String bankname) {
        this.bankname = bankname;
    }

    public String getBankaccountno() {
        return bankaccountno;
    }

    public void setBankaccountno(String bankaccountno) {
        this.bankaccountno = bankaccountno;
    }

    public Integer getInvoicetype() {
        return invoicetype;
    }

    public void setInvoicetype(Integer invoicetype) {
        this.invoicetype = invoicetype;
    }

    public String getMerpubkey() {
        return merpubkey;
    }

    public void setMerpubkey(String merpubkey) {
        this.merpubkey = merpubkey;
    }

    public String getGdsypubkey() {
        return gdsypubkey;
    }

    public void setGdsypubkey(String gdsypubkey) {
        this.gdsypubkey = gdsypubkey;
    }

    public String getGdsyprikey() {
        return gdsyprikey;
    }

    public void setGdsyprikey(String gdsyprikey) {
        this.gdsyprikey = gdsyprikey;
    }

    public String getDeskey() {
        return deskey;
    }

    public void setDeskey(String deskey) {
        this.deskey = deskey;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Date getCreatedate() {
        return createdate;
    }

    public void setCreatedate(Date createdate) {
        this.createdate = createdate;
    }

    public Integer getBusinessmode() {
        return businessmode;
    }

    public void setBusinessmode(Integer businessmode) {
        this.businessmode = businessmode;
    }

    public String getOucode() {
        return oucode;
    }

    public void setOucode(String oucode) {
        this.oucode = oucode;
    }

    public String getAccountowner() {
        return accountowner;
    }

    public void setAccountowner(String accountowner) {
        this.accountowner = accountowner;
    }

    public Integer getIsinner() {
        return isinner;
    }

    public void setIsinner(Integer isinner) {
        this.isinner = isinner;
    }

    public Integer getIspref() {
        return ispref;
    }

    public void setIspref(Integer ispref) {
        this.ispref = ispref;
    }

    public String getCreateby() {
        return createby;
    }

    public void setCreateby(String createby) {
        this.createby = createby;
    }

    public String getContracturl() {
        return contracturl;
    }

    public void setContracturl(String contracturl) {
        this.contracturl = contracturl;
    }

    public String getValiddays() {
        return validdays;
    }

    public void setValiddays(String validdays) {
        this.validdays = validdays;
    }

    public String getJmcode() {
        return jmcode;
    }

    public void setJmcode(String jmcode) {
        this.jmcode = jmcode;
    }

    public Integer getInvoicedate() {
        return invoicedate;
    }

    public void setInvoicedate(Integer invoicedate) {
        this.invoicedate = invoicedate;
    }

    public String getInvoiceeffectdate() {
        return invoiceeffectdate;
    }

    public void setInvoiceeffectdate(String invoiceeffectdate) {
        this.invoiceeffectdate = invoiceeffectdate;
    }

    public String getInvoicecontactname() {
        return invoicecontactname;
    }

    public void setInvoicecontactname(String invoicecontactname) {
        this.invoicecontactname = invoicecontactname;
    }

    public String getInvoicecontacttel() {
        return invoicecontacttel;
    }

    public void setInvoicecontacttel(String invoicecontacttel) {
        this.invoicecontacttel = invoicecontacttel;
    }

    public String getInvoicecontactaddress() {
        return invoicecontactaddress;
    }

    public void setInvoicecontactaddress(String invoicecontactaddress) {
        this.invoicecontactaddress = invoicecontactaddress;
    }
}