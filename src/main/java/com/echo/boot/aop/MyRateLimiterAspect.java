package com.echo.boot.aop;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;

import java.lang.reflect.Method;

/**
 * Created with IntelliJ IDEA
 * Created By CQ
 * Date: 2020/7/10
 * Time: 7:57
 */
@Aspect
public class MyRateLimiterAspect {


    @Pointcut("execution(* com.echo.boot.controller.UserController.*.*(..))")
    public void rateLimiterPointCut() {

    }

    @Around("rateLimiterPointCut()")
    public Object doAround(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        Method method = getSignatureMethod(proceedingJoinPoint);
        Object proceed = proceedingJoinPoint.proceed();
        return proceed;
    }

    private Method getSignatureMethod(ProceedingJoinPoint proceedingJoinPoint) {
        return ((MethodSignature) proceedingJoinPoint.getSignature()).getMethod();
    }

}
