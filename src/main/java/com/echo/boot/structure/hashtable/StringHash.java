package com.echo.boot.structure.hashtable;

import java.util.Hashtable;

/**
 * Created with IntelliJ IDEA
 * Created By CQ
 * Date: 2020/6/4
 * Time: 9:54
 * 字符串获得hash
 */
public class StringHash {

    public static void main(String[] args) {
        Hashtable<String, String> hashtable = new Hashtable<>();
        // 尽量让每个字符串参与运算
        String str = "jack";
        int len = str.length();
        int hashCode = 0;

        for (int i = 0; i < len; i++) {
            char c = str.charAt(i);
            hashCode = hashCode * 31 + c;
//            hashCode = (hashCode << 5) - hashCode + c;
        }

        int result = 1;


        char[] a = str.toCharArray();
        Integer.hashCode(a[1]);
        for (Object element : a) {
            result = 31 * result + (element == null ? 0 : element.hashCode());
        }

        /**
         * 第一次 i = 0;
         * hashCode = j
         * 第二次
         * hashCode = (j * 31) + a
         * 第三次
         * hashCode = ((j * 31) + a ) * 31 + c
         * 第四次
         * hashCode = (((j * 31) + a ) * 31 + c) * 31 + k
         */
        //3254239
        System.out.println(hashCode);
        System.out.println(result);
    }

    static void test() {
        Integer a = 10;
        Long b = 12L;
        Double c = 12.34;
        Float d = 12.67F;
        String e = "jack";

        System.out.println(a.hashCode());
        System.out.println(b.hashCode());
        System.out.println(c.hashCode());
        System.out.println(d.hashCode());
        System.out.println(e.hashCode());


    }
}
