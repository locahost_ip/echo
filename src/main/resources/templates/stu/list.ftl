<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<h4>欢迎:${loginName},年龄：${loginage}</h4>

<table>
    <thead>
    <tr>
        <th>id</th>
        <th>姓名</th>
        <th>性别</th>
    </tr>
    </thead>
    <tbody>
    <#list studentList?sort_by("id") ? reverse as stu>
        <tr>
            <td>${stu.id}</td>
            <td>${stu.userName}</td>
            <td>${stu.gender}</td>
        </tr>
    </#list>
    </tbody>
</table>
</body>
</html>