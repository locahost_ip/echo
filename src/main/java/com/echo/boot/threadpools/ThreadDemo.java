package com.echo.boot.threadpools;

import com.google.common.util.concurrent.ThreadFactoryBuilder;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicStampedReference;

/**
 * Created with IntelliJ IDEA
 * Created By CQ
 * Date: 2020/4/14
 * Time: 18:57
 */
public class ThreadDemo {
    public static void main(String[] args) {
//        AtomicInteger integer = new AtomicInteger(0);
        int stamp = 1;
        AtomicStampedReference<Integer> reference = new AtomicStampedReference<>(5, 1);
        reference.compareAndSet(0, 10, stamp, stamp + 1);
        System.out.println(reference.getReference());
        System.out.println(reference.getStamp());
//        ThreadPoolExecutor executor = new ThreadPoolExecutor(4, 8, 60, TimeUnit.SECONDS, new LinkedBlockingQueue<Runnable>(1024), new ThreadFactoryBuilder().setNameFormat("CHEN_QUN-Thread-%d").build());
//        for (int i = 0; i < 400; i++) {
//            executor.execute(integer::incrementAndGet);
//        }
//        System.out.println(integer.get());
//        executor.shutdown();
    }
}
