package com.echo.boot.advance;

import java.util.Random;

/**
 * Created with IntelliJ IDEA
 * Created By CQ
 * Date: 2020/9/11
 * Time: 16:53
 */
public class RandomTest {
    public static void main(String[] args) {
        // 生成 0 - 99 的数
        // [0,100);
        int i = new Random().nextInt(99);
        // Math.random() [0,1.0);
        int i2 = (int) (Math.random() * 100);

        // 生成 10 - 99
        int i3 = (int) (new Random().nextInt(90)) + 10;
        int i4 = (int) (Math.random() * 90) + 10;
    }
}
