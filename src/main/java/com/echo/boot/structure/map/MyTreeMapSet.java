package com.echo.boot.structure.map;

import com.echo.boot.structure.tree.binarytree.Visitor;
import com.echo.boot.structure.set.Set;

/**
 * Created with IntelliJ IDEA
 * Created By CQ
 * Date: 2020/6/3
 * Time: 12:07
 * 用 map 实现 set
 * node 节点 存储 的是 key = element,value = null;
 */
public class MyTreeMapSet<E> implements Set<E> {
    Map<E, Object> map = new MyTreeMap<E, Object>();

    @Override
    public int size() {
        return map.size();
    }

    @Override
    public boolean isEmpty() {
        return map.isEmpty();
    }

    @Override
    public void clear() {
        map.clear();
    }

    @Override
    public boolean contains(E element) {
        return map.containsKey(element);
    }

    @Override
    public void add(E element) {
        map.put(element, null);
    }

    @Override
    public void remove(E element) {
        map.remove(element);
    }

    @Override
    public void traversal(Visitor<E> visitor) {
        if (visitor == null) {
            return;
        }
        map.traversal(new Map.MapVisitor<E, Object>() {
            @Override
            public boolean visit(E key, Object value) {
                visitor.visit(key);
                return false;
            }
        });
    }
}
