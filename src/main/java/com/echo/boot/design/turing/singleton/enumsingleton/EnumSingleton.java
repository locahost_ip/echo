package com.echo.boot.design.turing.singleton.enumsingleton;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

/**
 * Created with IntelliJ IDEA
 * Created By CQ
 * Date: 2020/7/26
 * Time: 10:13
 */
public enum EnumSingleton {
    /**
     * 单例模式
     */
    INSTANCE;

    public static void main(String[] args) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        EnumSingleton instance = EnumSingleton.INSTANCE;
        EnumSingleton instance1 = EnumSingleton.INSTANCE;
        System.out.println(instance == instance1);

        // 反射攻击  枚举 是无法通过反射来获取实例的,单例也是通过静态代码块来实现的,静态代码块也是JVM 保证的,线程也是安全的

        Constructor<EnumSingleton> declaredConstructor = EnumSingleton.class.getDeclaredConstructor(String.class, int.class);
        declaredConstructor.setAccessible(true);
        EnumSingleton instance2 = declaredConstructor.newInstance("INSTANCE", 0);


    }
}

