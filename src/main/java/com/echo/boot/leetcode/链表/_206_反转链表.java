package com.echo.boot.leetcode.链表;

/**
 * Created with IntelliJ IDEA
 * Created By CQ
 * Date: 2020/5/2
 * Time: 19:00
 */
public class _206_反转链表 {
    public static void main(String[] args) {

    }


    public ListNode reverseList(ListNode head) {
        if (head == null || head.next == null) {
            return head;
        }
        ListNode newHead = reverseList(head);
        head.next.next = head;
        head.next = null;
        return newHead;
    }

    public ListNode revdrseList2(ListNode head) {
        if (head == null || head.next == null) {
            return head;
        }
        ListNode newHead = null;
        while (head != null) {
            // temp  = 4
            ListNode temp = head.next;
            // 5.next = null;
            head.next = newHead;
            // newHead = 5;
            newHead = head;
            // head = 4
            head = temp;
        }
        return newHead;
    }
}
