package com.echo.boot.utils;

import javax.crypto.KeyGenerator;
import javax.crypto.Mac;
import javax.crypto.SecretKey;
import java.math.BigInteger;
import java.util.Arrays;

/**
 * Created with IntelliJ IDEA
 * Created By CQ
 * Date: 2020/3/27
 * Time: 10:17
 * 摘要算法
 * 也叫 哈希算法 或者 数字指纹（Digest）：
 */
public class HmacMD5 {
    // 说明 HmacMD5
    // ，存储用户的哈希口令时，要加盐存储，目的就在于抵御彩虹表攻击。
    //因此，HmacMD5可以看作带有一个安全的key的MD5。使用HmacMD5而不是用MD5加salt，有如下好处：

    /**
     * HmacMD5使用的key长度是64字节，更安全；
     * Hmac是标准算法，同样适用于SHA-1等其他哈希算法；
     * Hmac输出和原有的哈希算法长度一致。
     */

    // 记得保存 随机的  skey
    public static void main(String[] args) throws Exception {
        KeyGenerator keyGen = KeyGenerator.getInstance("HmacMD5");
        SecretKey key = keyGen.generateKey();
        // 打印随机生成的key:
        byte[] skey = key.getEncoded();
        System.out.println(Arrays.toString(skey));
        String s = new BigInteger(1, skey).toString(16);
        System.out.println(s);
        Mac mac = Mac.getInstance("HmacMD5");
        mac.init(key);
        mac.update("HelloWorld".getBytes("UTF-8"));
        byte[] result = mac.doFinal();
        System.out.println(new BigInteger(1, result).toString(16));
    }
}
