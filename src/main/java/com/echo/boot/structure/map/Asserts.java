package com.echo.boot.structure.map;

/**
 * Created with IntelliJ IDEA
 * Created By CQ
 * Date: 2020/6/11
 * Time: 9:40
 */
public class Asserts {
    public static void test(boolean value) {
        try {
            if (!value) {
                throw new Exception("测试未通过");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
