package com.echo.boot.design.turing.factory;

/**
 * Created with IntelliJ IDEA
 * Created By CQ
 * Date: 2020/8/8
 * Time: 7:59
 * 提供 一个创建一系列相关或相互依赖的接口, 而无需指定它们的具体实现
 * 抽象工厂模式
 *
 * @author Administrator
 */
public class AbstractFactoryMetod {
    public static void main(String[] args) {
//        IdatabaseUtils utils = new ImySqlUtils();
        IdatabaseUtils utils = new IoracleUtils();
        Iconnection connection = utils.getConnection();
        connection.connect();
        Icommad commad = utils.getCommad();
        commad.commad();
    }
}


interface Iconnection {
    /**
     * 数据库链接
     */
    void connect();
}

interface Icommad {
    /**
     * 数据库命令
     */
    void commad();
}

// 一系列相关或相互依赖的接口,这就是抽象工厂,具体实现就给子类
interface IdatabaseUtils {
    Iconnection getConnection();

    Icommad getCommad();
}

// MySql 实现
class ImySqlConnection implements Iconnection {

    @Override
    public void connect() {
        System.out.println("ImySql Connected ...");
    }
}


// mysql 实现 开始
class ImysqlCommand implements Icommad {

    @Override
    public void commad() {
        System.out.println("ImySql command executed.....");
    }
}

class ImySqlUtils implements IdatabaseUtils {

    @Override
    public Iconnection getConnection() {
        return new ImySqlConnection();
    }

    @Override
    public Icommad getCommad() {
        return new ImysqlCommand();
    }
}
// mysql 实现 结束

// oracle 实现开始
class IoracleConnection implements Iconnection {

    @Override
    public void connect() {
        System.out.println("ioracle connected ....");
    }
}

class IoracleCommand implements Icommad {
    @Override
    public void commad() {
        System.out.println("Ioracle command executed .....");
    }
}

class IoracleUtils implements IdatabaseUtils {

    @Override
    public Iconnection getConnection() {
        return new IoracleConnection();
    }

    @Override
    public Icommad getCommad() {
        return new IoracleCommand();
    }
}
