package com.echo.boot.error;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

/**
 * Created with IntelliJ IDEA
 * Created By CQ
 * Date: 2020/1/21
 * Time: 20:37
 * 全局捕获异常
 * 1.返回Json 格式
 * 2.返回页面
 *
 * @author Administrator
 * @ControllerAdvice("扫包范围")
 */
@ControllerAdvice("com.echo.myspringboot.controller")
public class GlobalExceptionHandler {
    private static final Logger logger = LoggerFactory.getLogger(GlobalExceptionHandler.class);

//    @ExceptionHandler(RuntimeException.class)
//    @ResponseBody
//    public Map<String,Object> exceptionHandler(){
//        HashMap<String, Object> map = new HashMap<>();
//        map.put("errorCode","500");
//        map.put("errorMsg","全局捕获异常");
//        return map;
//    }

    @ExceptionHandler(RuntimeException.class)
    public ModelAndView defaultErrorHandler(HttpServletRequest request, Exception e) {
        ModelAndView mav = new ModelAndView();
        mav.addObject("exception", e);
        mav.addObject("url", request.getRequestURL());
        mav.setViewName("common/error");
        return mav;
    }


}

