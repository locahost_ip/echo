package com.echo.boot.design.turing.factory;

/**
 * Created with IntelliJ IDEA
 * Created By CQ
 * Date: 2020/8/7
 * Time: 20:48
 * 工厂方法模式
 */
public class Factory {
    public static void main(String[] args) {
        Product p1 = new FactoryProductM().createProduck();
        p1.method();
        Product p2 = new FactoryProductN().createProduck();
        p2.method();
    }

}

interface Product {
    // 所有的产品都必须实现
    void method();
}

//把具体的实现交个子类
// 这就是工厂,生产一系列的产品的,把具体实现交给子类
abstract class Applications {
    // 把产品的具体实现就给子类,需要那个产品就创造那个产品。
    abstract Product createProduck();

    Product getProduct() {
        // 一大段逻辑
        // ........
        return createProduck();
    }
}

// 产品 M
class ProductM implements Product {
    // 不变的是系类产品,变得是产品的 方法
    @Override
    public void method() {
        System.out.println("product M .method executed ...");
    }

}

class ProductN implements Product {

    @Override
    public void method() {
        System.out.println("product N .method executed ...");
    }
}

class FactoryProductM extends Applications {
    // 不变的是产品,变的是 需要那个产品

    @Override
    Product createProduck() {
        // 具体需要创建的 产品在这里实现
        return new ProductM();
    }
}

class FactoryProductN extends Applications {

    @Override
    Product createProduck() {
        return new ProductN();
    }
}
