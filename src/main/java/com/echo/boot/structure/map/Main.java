package com.echo.boot.structure.map;

import com.echo.boot.pojo.Key;
import com.echo.boot.structure.tree.printer.BinaryTrees;

/**
 * Created with IntelliJ IDEA
 * Created By CQ
 * Date: 2020/6/3
 * Time: 11:30
 */
public class Main {
    public static void main(String[] args) {
//        myTreeMapTest();
        test4(new MyHashMap<Object, Integer>());
    }

    static void myTreeMapTest() {
        Map<String, Integer> map = new MyTreeMap<>();
        map.put("a", 2);
        map.put("b", 4);
        map.put("c", 7);
        map.put("d", 9);
        System.out.println(map.size());
        map.traversal(new Map.MapVisitor<String, Integer>() {
            @Override
            public boolean visit(String key, Integer value) {
                System.out.println("key : " + key + " ,value : " + value);
                return false;
            }
        });
        BinaryTrees.print(map);
    }

    static void test4(MyHashMap<Object, Integer> map) {
//        map.put("jack", 1);
//        map.put("rose", 2);
//        map.put("jim", 3);
//        map.put("jake", 4);
//        System.out.println(map.size());
//        System.out.println(map.get("jake"));
//        map.remove("jack");
//        map.remove("jim");
//        System.out.println(map.size());

        for (int i = 1; i <= 10; i++) {
//            map.put("test" + i, i);
            map.put(new Key(i), i);
        }
        BinaryTrees.print(map);
        for (int i = 5; i <= 7; i++) {
            System.out.println(i);
            Integer integer = map.remove(new Key(i));
            System.out.println(integer);
//
//            Asserts.test(integer == i);
        }
//        for (int i = 1; i <= 3; i++) {
//            map.put(new Key(i), i + 5);
//        }
//        Asserts.test(map.size() == 19);
//        Asserts.test(map.get(new Key(1)) == 6);
//        Asserts.test(map.get(new Key(2)) == 7);
//        Asserts.test(map.get(new Key(3)) == 8);
//        Asserts.test(map.get(new Key(4)) == 4);
//        Asserts.test(map.get(new Key(5)) == null);
//        Asserts.test(map.get(new Key(6)) == null);
//        Asserts.test(map.get(new Key(7)) == null);
//        Asserts.test(map.get(new Key(8)) == 8);
//        map.traversal(new Map.MapVisitor<Object, Integer>() {
//            @Override
//            public boolean visit(Object key, Integer value) {
//                System.out.println(key + "_" + value);
//                return false;
//            }
//        });
    }
}

