package com.echo.boot.design.turing.facade;

/**
 * Created with IntelliJ IDEA
 * Created By CQ
 * Date: 2020/8/18
 * Time: 23:23
 * 为子系统中的一组接口提供一个一致的界面。Facade模式定义了一个高层接口，这个接口使得这一子系统更加容易使用
 */
public class Facade {
    private System1 system1 = new System1();
    private System2 system2 = new System2();
    private System3 system3 = new System3();

    public void doSomethingFacade() {
        system1.method1();
        system2.method2();
        system3.method3();
    }

}


class System1 {
    public void method1() {
        System.out.println("system 1 method ");
    }
}

class System2 {
    public void method2() {
        System.out.println("system 2 method ");
    }
}

class System3 {
    public void method3() {
        System.out.println("systome 3 method ");
    }
}


class Client1 {
    //    private System1 system1 = new System1();
//    private System2 system2 = new System2();
//    private System3 system3 = new System3();
    private Facade facade = new Facade();

    public void doSomething1() {
//        system1.method1();
//        system2.method2();
//        system3.method3();
        facade.doSomethingFacade();
    }
}

class Client2 {
    //    private System1 system1 = new System1();
//    private System2 system2 = new System2();
//    private System3 system3 = new System3();
    private Facade facade = new Facade();

    public void doSomething2() {
//        system1.method1();
//        system2.method2();
//        system3.method3();
        facade.doSomethingFacade();
    }

}

