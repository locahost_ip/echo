package com.echo.boot.sync.waitandnotify;

/**
 * Created with IntelliJ IDEA
 * Created By CQ
 * Date: 2020/9/13
 * Time: 22:02
 */
public class Main {
    public static void main(String[] args) {
        Drop drop = new Drop();
        (new Thread(new Consumer(drop))).start(); // 开启消费者线程
        (new Thread(new Producer(drop))).start(); // 开启生产者线程
    }
}
