package com.echo.boot.enumeration;

/**
 * Created with IntelliJ IDEA
 * Created By CQ
 * Date: 2020/7/3
 * Time: 15:39
 * <p>
 * 所有的枚举值都是 public static final 的，且非抽象的枚举类不能再派生子类
 *
 * @author Administrator
 */
public enum Color implements ColorDescription {
    /**
     * 红色
     */
    RED("红色", "#f30606"),
    /**
     * 绿色
     */
    GREEN("绿色", "#56f306"),
    /**
     * 黄色
     */
    YELLE("黄色", "#f3d606");

    /**
     * 颜色的名字
     */
    private String name;

    /**
     * 颜色代号
     */
    private String number;

    Color(String name, String number) {
        this.name = name;
        this.number = number;
    }

    @Override
    public void info() {
        System.out.println("颜色" + name + "色号" + number);
    }
}
