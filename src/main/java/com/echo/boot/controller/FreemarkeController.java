package com.echo.boot.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created with IntelliJ IDEA
 * Created By CQ
 * Date: 2020/1/19
 * Time: 21:57
 */
@Controller
public class FreemarkeController {
    // springboot 整合 freemarker
    // properties 配置：
    // spring.freemarker.suffix=.ftl
    @RequestMapping("index")
    public String indexFtl() {
        return "index";
    }
}
