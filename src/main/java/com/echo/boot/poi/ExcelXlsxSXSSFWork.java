package com.echo.boot.poi;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;

import javax.swing.filechooser.FileSystemView;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * Created with IntelliJ IDEA
 * Created By CQ
 * Date: 2020/10/28
 * Time: 10:08
 *
 * @author Administrator
 */
public class ExcelXlsxSXSSFWork {
    public static void main(String[] args) throws IOException {
        writeBigExcelXlsx();
    }

    static void writeBigExcelXlsx() throws IOException {
        long begin = System.currentTimeMillis();
        FileSystemView fsv = FileSystemView.getFileSystemView();
        String desktop = fsv.getHomeDirectory().getPath();
        String filePath = desktop + "/template.xlsx";
        File file = new File(filePath);
        OutputStream outputStream = new FileOutputStream(file);
        Workbook workbook = new SXSSFWorkbook();
        Sheet sheet = workbook.createSheet();
        for (int rowNum = 0; rowNum < 100000; rowNum++) {
            Row row = sheet.createRow(rowNum);
            for (int cellNum = 0; cellNum < 10; cellNum++) {
                Cell cell = row.createCell(cellNum);
                cell.setCellValue(cellNum);
            }
        }
        System.out.println("done");
        workbook.write(outputStream);
        outputStream.close();
        ((SXSSFWorkbook) workbook).dispose();
        long end = System.currentTimeMillis();

        System.out.println((double) (end - begin) / 100);
    }

}
