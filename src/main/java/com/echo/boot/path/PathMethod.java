package com.echo.boot.path;

import java.io.File;
import java.net.URL;

public class PathMethod {
    public static void main(String[] args) {
        File file = new File("");
        System.out.println(file.getPath());
        File file1 = new File("");
        System.out.println(file1);

        URL resource = PathMethod.class.getClassLoader().getResource("");
        System.out.println(resource);

    }
}
