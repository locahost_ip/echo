package com.echo.boot.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created with IntelliJ IDEA
 * Created By CQ
 * Date: 2020/1/20
 * Time: 19:25
 * springboot 2.0 整合 jsp
 * pom 依赖 2 个
 * 具体流程
 * 1.src\main\ 目录下新建 webapp 目录
 * 2.Ctrl+Shift+Alt+s 项目配置（project setting）的 module 中 添加web 项目
 * 3.web 资源目录指向 webapp 目录
 */
@Controller
@Slf4j
public class JspController {
    // springboot 整合jsp
    // properties 配置：
    // #spring.mvc.view.prefix=/WEB-INF/jsp/
    //#spring.mvc.view.suffix=.jsp
    // pom文件已注释 依赖 
    @RequestMapping("indexjsp")
    public String indexJsp() {

        return "index";
    }
}
