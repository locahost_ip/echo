package com.echo.boot.pojo;

import com.echo.boot.annotation.MyAnnotation;
import com.echo.boot.annotation.OnAnnotation;
import com.echo.boot.enumeration.Color;

/**
 * Created with IntelliJ IDEA
 * Created By CQ
 * Date: 2020/7/3
 * Time: 21:18
 *
 * @author Administrator
 */
@MyAnnotation(name = "ANNO", age = 22, color = Color.RED, onAnnotation = @OnAnnotation, strs = {"A", "B", "C"})
public class Anno {

    private String name;

    private int age;

    private Color color;


}
