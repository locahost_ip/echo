package com.echo.boot.proxy;

import com.echo.boot.pojo.Theme;
import com.echo.boot.service.ThemeService;
import com.echo.boot.service.imp.ThemeServiceImp;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * Created with IntelliJ IDEA
 * Created By CQ
 * Date: 2020/7/2
 * Time: 12:07
 */
public class MyInvocation implements InvocationHandler {

    private MyAspect myAspect = new MyAspect();

    private Object target;

    public MyInvocation(Object target) {
        this.target = target;
    }


    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        System.out.println(proxy.getClass().getName());
        myAspect.befor();
        Object invoke = method.invoke(target, args);
        myAspect.after();
        return invoke;
    }

    public static void main(String[] args) {
        // jdk动态代理，必须是面向接口，目标业务类必须实现接口
        ClassLoader classLoader = MyInvocation.class.getClassLoader();
        ThemeServiceImp imp = new ThemeServiceImp();
        MyInvocation invocation = new MyInvocation(imp);
        ThemeService o = (ThemeService) Proxy.newProxyInstance(classLoader, imp.getClass().getInterfaces(), invocation);
        int insert = o.insert(new Theme());
        System.out.println(insert);
    }

}
