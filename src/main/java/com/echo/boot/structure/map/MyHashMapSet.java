package com.echo.boot.structure.map;

import com.echo.boot.structure.tree.binarytree.Visitor;
import com.echo.boot.structure.set.Set;

/**
 * Created with IntelliJ IDEA
 * Created By CQ
 * Date: 2020/6/13
 * Time: 9:54
 */
public class MyHashMapSet<E> implements Set<E> {
    private MyHashMap<E, Object> map = new MyHashMap<>();

    @Override
    public int size() {
        return map.size();
    }

    @Override
    public boolean isEmpty() {
        return map.isEmpty();
    }

    @Override
    public void clear() {
        map.clear();
    }

    @Override
    public boolean contains(E element) {
        return map.containsKey(element);
    }

    @Override
    public void add(E element) {
        map.put(element, null);
    }

    @Override
    public void remove(E element) {
        map.remove(element);
    }

    @Override
    public void traversal(Visitor<E> visitor) {
        map.traversal(new Map.MapVisitor<E, Object>() {
            @Override
            public boolean visit(E key, Object value) {
                return visitor.visit(key);
            }
        });
    }
}
