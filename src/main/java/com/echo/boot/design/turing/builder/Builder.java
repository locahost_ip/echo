package com.echo.boot.design.turing.builder;

/**
 * Created with IntelliJ IDEA
 * Created By CQ
 * Date: 2020/8/8
 * Time: 23:18
 * 建造者模式
 * 将一个复杂对象的创建与它的表示分离，使得同样的构造过程可以创建不同的表示
 * <p>
 * product 复杂对象
 * Builder 抽象构造
 * DefaultConcreteBuilder  抽象构造具体的实现方式
 * Director 控制构建的步骤
 */
public class Builder {
    public static void main(String[] args) {
        ProductBuilder builder = new DefaultConcreteBuilder();
        Director director = new Director(builder);
        Product product = director.makeProduct("productName", "companyName", "part1", "part2", "part3", "part4");


        System.out.println(product);
    }
}


interface ProductBuilder {
    void builderProductName(String productName);

    void builderCompanyName(String companyName);

    void builderpart1(String part1);

    void builderpart2(String part2);

    void builderpart3(String part3);

    void builderpart4(String part4);

    Product build();
}

// 默认构造方法处理
class DefaultConcreteBuilder implements ProductBuilder {
    private String productName;
    private String companyName;
    private String part1;
    private String part2;
    private String part3;
    private String part4;

    @Override
    public void builderProductName(String productName) {
        this.productName = productName;
    }

    @Override
    public void builderCompanyName(String companyName) {
        this.companyName = companyName;
    }

    @Override
    public void builderpart1(String part1) {
        this.part1 = part1;
    }

    @Override
    public void builderpart2(String part2) {
        this.part2 = part2;
    }

    @Override
    public void builderpart3(String part3) {
        this.part3 = part3;
    }

    @Override
    public void builderpart4(String part4) {
        this.part4 = part4;
    }

    @Override
    public Product build() {
        return new Product(this.productName, this.companyName, this.part1, this.part2, this.part3, this.part4);
    }
}

// 只要实现ProductBuilder 就可实现具体的构造产品方式
class SpecialConcreteBuiler implements ProductBuilder {
    private String productName;
    private String companyName;
    private String part1;
    private String part2;
    private String part3;
    private String part4;

    @Override
    public void builderProductName(String productName) {
        this.productName = productName;
    }

    @Override
    public void builderCompanyName(String companyName) {
        this.companyName = companyName;
    }

    @Override
    public void builderpart1(String part1) {
        this.part1 = part1;
    }

    @Override
    public void builderpart2(String part2) {
        this.part2 = part2;
    }

    @Override
    public void builderpart3(String part3) {
        this.part3 = part3;
    }

    @Override
    public void builderpart4(String part4) {
        this.part4 = part4;
    }

    @Override
    public Product build() {
        return new Product(this.productName, this.companyName, this.part1, this.part2, this.part3, this.part4);
    }
}

class Director {
    private ProductBuilder builder;

    public Director(ProductBuilder builder) {
        this.builder = builder;
    }

    public Product makeProduct(String productName, String companyName, String part1, String part2, String part3, String part4) {
        builder.builderProductName(productName);
        builder.builderCompanyName(companyName);
        builder.builderpart1(part1);
        builder.builderpart2(part2);
        builder.builderpart3(part3);
        builder.builderpart4(part4);
        return builder.build();
    }
}

/**
 * 需要构造的产品需要 一定顺序构造
 */
class Product {
    private String productName;
    private String companyName;
    private String part1;
    private String part2;
    private String part3;
    private String part4;

    /**
     * 无参构造
     */
    public Product() {
    }


    public Product(String productName, String companyName, String part1, String part2, String part3, String part4) {
        this.productName = productName;
        this.companyName = companyName;
        this.part1 = part1;
        this.part2 = part2;
        this.part3 = part3;
        this.part4 = part4;

    }

    @Override
    public String toString() {
        return "Product{" +
                "productName='" + productName + '\'' +
                ", companyName='" + companyName + '\'' +
                ", part1='" + part1 + '\'' +
                ", part2='" + part2 + '\'' +
                ", part3='" + part3 + '\'' +
                ", part4='" + part4 + '\'' +
                '}';
    }

}
