package com.echo.boot.structure.linearlist.list;

/**
 * Created with IntelliJ IDEA
 * Created By CQ
 * Date: 2020/5/1
 * Time: 17:36
 * 让子类实现接口
 */
public interface MyList<E> {

    /**
     * 常量 -1 在数组中没要找到元素 返回 -1
     * 没找到元素返回 -1;
     */
    static final int ELEMENT_NOT_FOUND = -1;

    /**
     * 返回数组长度
     *
     * @return 数组长度
     */
    int size();

    /**
     * @return 数组是否为空
     */
    boolean isEmpty();

    /**
     * 数组是否包含传入的元素
     *
     * @param element 传入的元素
     * @return 是否包含元素
     */
    boolean contains(E element);

    /**
     * 想数组中加入元素,直接添加在末尾
     *
     * @param element 添加元素,向末尾添加
     */
    void add(E element);

    /**
     * 清空数组
     */
    void clear();

    /**
     * @param index 传入索引
     * @return 返回对应索引的值.
     */
    E get(int index);

    /**
     * 返回修改元素的
     *
     * @param index   索引
     * @param element 元素
     * @return
     */
    E set(int index, E element);

    /**
     * @param index   在那个索引的位置添加
     * @param element 元素
     */
    void add(int index, E element);

    /**
     * @param index 删除指定索引的值
     * @return 返回被删除的元素
     */
    E remove(int index);

    /**
     * @param element 传入元素
     * @return 返回第一次出现索引位置
     */
    int indexOf(E element);
}
