package com.echo.boot.proxy;

/**
 * Created with IntelliJ IDEA
 * Created By CQ
 * Date: 2020/7/2
 * Time: 12:00
 *
 * @author Administrator
 */
public class MyAspect {
    public void befor() {
        System.out.println("开启事务");
    }

    public void after() {
        System.out.println("提交事务");
    }
}
