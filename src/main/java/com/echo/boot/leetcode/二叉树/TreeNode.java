package com.echo.boot.leetcode.二叉树;

/**
 * Created with IntelliJ IDEA
 * Created By CQ
 * Date: 2020/5/18
 * Time: 14:24
 */
public class TreeNode {
    int val;
    TreeNode left;
    TreeNode right;

    TreeNode(int val) {
        this.val = val;
    }
}
