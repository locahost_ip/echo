package com.echo.boot.advance;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA
 * Created By CQ
 * Date: 2020/9/11
 * Time: 17:28
 */
public class ArrayListTest {
    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();
        list.add(11);
        list.add(22);
        list.add(33);

        list.toArray(new Integer[0]);
    }
}
