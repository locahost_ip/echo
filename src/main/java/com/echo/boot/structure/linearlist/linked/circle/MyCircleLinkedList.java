package com.echo.boot.structure.linearlist.linked.circle;

import com.echo.boot.structure.linearlist.list.MyAbstractList;

/**
 * Created with IntelliJ IDEA
 * Created By CQ
 * Date: 2020/5/4
 * Time: 10:22
 * 循环双向链表
 * 第一个元素的prev 指向 最后一个元素
 * 最后一个元素的next 指向 第一个元素
 *
 * @author CQ
 */
public class MyCircleLinkedList<E> extends MyAbstractList<E> {
    private Node<E> first;
    private Node<E> last;
    private Node<E> current;

    private static class Node<E> {
        E element;
        Node<E> prev;
        Node<E> next;

        public Node(Node<E> prev, E element, Node<E> next) {
            this.element = element;
            this.prev = prev;
            this.next = next;
        }

        @Override
        public String toString() {
            StringBuilder builder = new StringBuilder();
            if (prev == null) {
                builder.append("null");
            } else {
                builder.append(prev.element);
            }
            builder.append("_").append(element).append("_");
            if (next == null) {
                builder.append("null");
            } else {
                builder.append(next.element);
            }
            return builder.toString();
        }
    }

    @Override
    public void clear() {
        // 这里可以不用每一个node销毁,源码里销毁是因为有迭代器的引用某一个元素，才把每一个node节点销毁
        // gc root 销毁栈内存对象引用堆内存。
        for (Node<E> x = first; x != null; ) {
            Node<E> next = x.next;
            x.element = null;
            x.next = null;
            x.prev = null;
            x = next;
        }
        size = 0;
        first = null;
        last = null;
    }

    @Override
    public E get(int index) {
        return node(index).element;
    }


    @Override
    public E set(int index, E element) {
        Node<E> node = node(index);
        E old = node.element;
        node.element = element;
        return old;
    }

    @Override
    public void add(int index, E element) {
        rangeCheckForAdd(index);
        // 获取当前节点元素
        // 往最后添加元素
        if (index == size) {
            Node<E> oldNode = last;
            last = new Node<E>(oldNode, element, first);
            // 添加第一个元素时
            // index = 0 ;size = 0;
            // 这是链表添加的第一个元素
            if (oldNode == null) {
                // 头,尾 都指向第一元素
                first = last;
                // 第一个元素的prev指向自己
                first.prev = first;
                // 第一个元素的next指向自己
                first.next = first;
            } else {
                // 最后一个节点.next  指向新创建的最后一个节点
                oldNode.next = last;
                // 第一个元素的 prev 指向 最后一个元素
                first.prev = last;
            }
        } else {
            // 获取当前元素
            Node<E> next = node(index);
            //  要把新的元素加到当前元素位置前面
            //  node.prev = 新的元素的prev
            // 新的元素的next = 当前节点
            // prev.next = 新的元素
            // node.prev = 新的元素
            // 边界处理 循环链表 的 头的prev 和 end.next = null;
            Node<E> prev = next.prev;
            Node<E> newNode = new Node<E>(prev, element, next);
            next.prev = newNode;
            prev.next = newNode;
            if (next == first) {
                first = newNode;
            }
        }
        size++;
    }

    @Override
    public E remove(int index) {
        rangeCheck(index);
        return remove(node(index));
    }

    private E remove(Node<E> node) {
        if (size == 1) {
            first = null;
            last = null;
        } else {
            Node<E> prev = node.prev;
            Node<E> next = node.next;
            prev.next = next;
            next.prev = prev;
            //  删除第一个元素
            if (node == first) {
                first = next;
            }
            // 删除最后一个元素
            if (node == last) {
                last = prev;
            }
        }
        size--;
        return node.element;
    }

    @Override
    public int indexOf(E element) {
        if (element == null) {
            Node<E> node = first;
            for (int i = 0; i < size; i++) {
                if (node.element == null) {
                    return i;
                }
                node = node.next;
            }
        } else {
            Node<E> node = first;
            for (int i = 0; i < size; i++) {
                if (element == node.element) {
                    return i;
                }
                node = node.next;
            }
        }
        return ELEMENT_NOT_FOUND;
    }

    /**
     * @author CQ
     * @DESCRIPTION: 获取当前节点
     * @params: [index]
     * @return: Node<E>
     * @Date: 2020/6/2 11:03
     * @Modified By:
     */
    private Node<E> node(int index) {
        rangeCheck(index);
        if (index < (size >> 1)) {
            Node<E> node = first;
            for (int i = 0; i < index; i++) {
                node = node.next;
            }
            return node;
        } else {
            Node<E> node = last;
            for (int i = size - 1; i > index; i--) {
                node = node.prev;
            }
            return node;
        }
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("size=").append(size);
        builder.append(", [");
        Node<E> node = first;
        for (int i = 0; i < size; i++) {
            if (i != 0) {
                builder.append(", ");
            }
            builder.append(node);
            node = node.next;
        }
        builder.append("]");
        return builder.toString();
    }

    public void reset() {
        current = first;
    }

    public E next() {
        if (current == null) {
            return null;
        }
        current = current.next;
        return current.element;
    }

    public E remove() {
        if (current == null) {
            return null;
        }
        Node<E> next = current.next;
        E element = remove(current);
        if (size == 0) {
            current = null;
        } else {
            current = next;
        }
        return element;
    }
}
