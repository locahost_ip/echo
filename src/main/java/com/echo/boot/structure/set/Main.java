package com.echo.boot.structure.set;

import com.echo.boot.structure.map.MyTreeMapSet;
import com.echo.boot.structure.tree.binarytree.Visitor;

/**
 * Created with IntelliJ IDEA
 * Created By CQ
 * Date: 2020/6/2
 * Time: 19:12
 */
public class Main {
    public static void main(String[] args) {
        myMapSet();
        myTreeSet();
    }

    static void myLinkedSet() {
        MyLinkedSet<Integer> set = new MyLinkedSet<>();
        set.add(10);
        set.add(20);
        set.add(30);
        set.add(10);
        set.add(10);

        System.out.println(set.size());
        set.traversal(new Visitor<Integer>() {
            @Override
            public boolean visit(Integer element) {
                System.out.println(element);
                return false;
            }
        });

        set.remove(10);
        System.out.println(set.size());
        set.traversal(new Visitor<Integer>() {
            @Override
            public boolean visit(Integer element) {
                System.out.println(element);
                return false;
            }
        });
    }

    static void myTreeSet() {
        // 红黑树
        MyTreeSet<Integer> set = new MyTreeSet<>();
        set.add(10);
        set.add(30);
        set.add(20);
        set.add(10);
        System.out.println(set.size());
        set.traversal(new Visitor<Integer>() {
            @Override
            public boolean visit(Integer element) {
                System.out.println(element);
                return false;
            }
        });
    }

    static void myMapSet() {
        // 红黑树
        MyTreeMapSet<Integer> set = new MyTreeMapSet<>();
        set.add(10);
        set.add(30);
        set.add(20);
        set.add(10);
        System.out.println(set.size());
        set.traversal(new Visitor<Integer>() {
            @Override
            public boolean visit(Integer element) {
                System.out.println(element);
                return false;
            }
        });
    }


}
