package com.echo.boot.sync.interview;

import java.util.concurrent.locks.LockSupport;

/**
 * Created with IntelliJ IDEA
 * Created By CQ
 * Date: 2020/4/16
 * Time: 21:45
 */
public class T02_00_LockSupport {
    static Thread t1, t2 = null;

    public static void main(String[] args) {

        char[] aI = "ABCDEFG".toCharArray();
        char[] aC = "1234567".toCharArray();

        t1 = new Thread(() -> {
            for (char c : aI) {
                System.out.println(c);
                LockSupport.unpark(t2);
                LockSupport.park();
            }
        }, "t1");


        t2 = new Thread(() -> {
            for (char c : aC) {
                LockSupport.park();
                LockSupport.unpark(t1);
                System.out.println(c);
            }
        }, "t2");

        t1.start();
        t2.start();

    }
}
