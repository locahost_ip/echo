package com.echo.boot.structure.linearlist.array;

import com.echo.boot.structure.linearlist.list.MyAbstractList;

/**
 * Created with IntelliJ IDEA
 * Created By CQ
 * Date: 2020/4/29
 * Time: 10:56
 * ArrayList 是否进一步优化
 * first = 索引第一个的位置
 */
public class MyArrayList<E> extends MyAbstractList<E> {

    private E[] elements;

    private static final int DEFAULT_CAPACITY = 10;

    public MyArrayList(int capacity) {
//        capacity = (capacity < DEFAULT_CAPACITY) ? DEFAULT_CAPACITY : capacity;
        capacity = Math.max(capacity, DEFAULT_CAPACITY);
        elements = (E[]) new Object[capacity];
    }

    public MyArrayList() {
        this(DEFAULT_CAPACITY);
    }


    /**
     * @author CQ
     * @DESCRIPTION: 向指定索引位置添加元素,
     * @params: [index, element]
     * @return: void
     * @Date: 2020/5/31 14:59
     * @Modified By:
     */
    @Override
    public void add(int index, E element) {
        rangeCheckForAdd(index);
        ensureCapacity(size + 1);
        // 索引后面的元素向后移动
        for (int i = size; i > index; i--) {
            elements[i] = elements[i - 1];
        }
        elements[index] = element;
        size++;
    }

    @Override
    public void clear() {
        for (int i = 0; i < size; i++) {
            elements[i] = null;
        }
        size = 0;
        if (elements != null && elements.length > DEFAULT_CAPACITY) {
            elements = (E[]) new Object[DEFAULT_CAPACITY];
        }
    }


    @Override
    public E get(int index) {
        rangeCheck(index);
        return elements[index];
    }

    @Override
    public E set(int index, E element) {
        rangeCheck(index);
        E old = elements[index];
        elements[index] = element;
        return old;
    }


    /**
     * @author CQ
     * @DESCRIPTION: 删除指定索引的值, 同时后面的元素向前移
     * @params: [index]
     * @return: E
     * @Date: 2020/5/31 10:45
     * @Modified By:  CQ
     */
    @Override
    public E remove(int index) {
        rangeCheck(index);
        E old = elements[index];
        // 索引后面的元素向前覆盖
        for (int i = index + 1; i < size; i++) {
            elements[i - 1] = elements[i];
        }
//        size--;
        //  索引后面的元素向前覆盖,最后一个元素是多余的,如果是引用类型的值,最后一个设置为null,垃圾回收
        elements[--size] = null;
        // 缩容开始
        trim();
        return old;
    }

    @Override
    public int indexOf(E element) {
        // 如果元素为 null,返回第一次出现 出现为 null 的元素
        if (element == null) {
            for (int i = 0; i < size; i++) {
                if (elements[i] == null) {
                    return i;
                }
            }
        } else {
            for (int i = 0; i < size; i++) {
                if (element.equals(elements[i])) {
                    return i;
                }
            }
        }
        return ELEMENT_NOT_FOUND;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("size=").append(size);
        builder.append(", [");
        for (int i = 0; i < size; i++) {
            if (i != 0) {
                builder.append(", ");
            }
            builder.append(elements[i]);
        }
        builder.append("]");
        return builder.toString();
    }

    private void ensureCapacity(int capacity) {
        int oldCapacity = elements.length;
        if (oldCapacity >= capacity) {
            return;
        }
//        int newCapacity = oldCapacity << 2;
        // 扩容原来的 1.5倍
        int newCapacity = oldCapacity + (oldCapacity >> 1);
        E[] newElements = (E[]) new Object[newCapacity];
//        System.arraycopy(elements, 0, newElements, 0, elements.length);
        for (int i = 0; i < size; i++) {
            newElements[i] = elements[i];
        }
        elements = newElements;
        System.out.println(capacity + "扩容为" + newCapacity + " size " + size);
    }

    // 缩容
    private void trim() {
        int capacity = elements.length;
//        if(oldCapacity >= (size << 1) || oldCapacity <= DEFAULT_CAPACITY){
//            return;
//        }
        int newCapacity = capacity >> 1;
        // 如果size 大于当前数组长度的 一半 或者 当前数组长度已经缩容到小于或等于 默认长度的时候不作处理
        if (size > newCapacity || capacity <= DEFAULT_CAPACITY) {
            return;
        }
        E[] newElements = (E[]) new Object[newCapacity];
        for (int i = 0; i < size; i++) {
            elements[i] = newElements[i];
        }
        elements = newElements;
        System.out.println(capacity + "缩容为" + newCapacity + " size " + size);

    }


}
