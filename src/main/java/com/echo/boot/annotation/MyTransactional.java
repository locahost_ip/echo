package com.echo.boot.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

/**
 * Created with IntelliJ IDEA
 * Created By CQ
 * Date: 2020/7/4
 * Time: 7:20
 *
 * @author Administrator
 */
@Target({ElementType.TYPE, ElementType.FIELD, ElementType.METHOD})
public @interface MyTransactional {

}
