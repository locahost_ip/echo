package com.echo.boot.pojo;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class Books {

    private String id;
    private String title;
    private BigDecimal price;
}
