package com.echo.boot.structure.map;

import com.echo.boot.structure.tree.printer.BinaryTreeInfo;

/**
 * Created with IntelliJ IDEA
 * Created By CQ
 * Date: 2020/6/2
 * Time: 21:03
 */
public interface Map<K, V> extends BinaryTreeInfo {
    /**
     * 键值对的个数
     *
     * @return
     */
    int size();

    /**
     * 是否为空
     *
     * @return
     */
    boolean isEmpty();

    /**
     * 清空所有元素
     */
    void clear();

    /**
     * 添加元素
     *
     * @param key
     * @param value
     * @return 返回原来的元素
     */
    V put(K key, V value);

    /**
     * 通过 键 获取 值;
     *
     * @param key
     * @return 值
     */
    V get(K key);

    /**
     * 通过键 删除 值, 删除被删除的 值
     *
     * @param key
     * @return
     */
    V remove(K key);

    /**
     * 是否包含 键
     *
     * @param key
     * @return
     */
    boolean containsKey(K key);

    /**
     * 是否包含 值
     *
     * @param value
     * @return
     */
    boolean containsValue(V value);

    void traversal(MapVisitor<K, V> visitor);

    public static abstract class MapVisitor<K, V> {
        boolean stop;

        public abstract boolean visit(K key, V value);
    }


}

