package com.echo.boot.structure.algorithm.sort.cmp;

import io.swagger.models.auth.In;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA
 * Created By CQ
 * Date: 2020/6/19
 * Time: 16:31
 */

public class Main {
    private static Map<Character, Integer> map = new HashMap<>(26);

    @Before
    public void before() {
        Character[] character = new Character[]{'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'};
        for (int i = 0; i < character.length; i++) {
            map.put(character[i], i + 1);
        }
    }

    @Test
    public void input() {
        String word = "banana";
        System.out.println("输入的是单词是 :" + word);
        StringBuilder builder = new StringBuilder();
        Integer sum= 0;
        for (int i = 0; i < word.length(); i++) {
            sum+=map.get(word.charAt(i));
            builder.append("(");
            builder.append(map.get(word.charAt(i)));
            builder.append(")+");
        }
        String s = builder.toString();
        s = s.substring(0,s.length()-1);
        System.out.println(s + "=" + "(" + sum + ")");
    }

}
