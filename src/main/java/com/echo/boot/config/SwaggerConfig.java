package com.echo.boot.config;

import com.github.xiaoymin.swaggerbootstrapui.annotations.EnableSwaggerBootstrapUI;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Created with IntelliJ IDEA
 * Created By CQ
 * Date: 2020/7/4
 * Time: 21:07
 *
 * @author Administrator
 */
@Configuration
@EnableSwagger2
@EnableSwaggerBootstrapUI
public class SwaggerConfig {

    public Docket creatResApi() {
        return new Docket(DocumentationType.SWAGGER_2).apiInfo(apiInfo()).select().apis(RequestHandlerSelectors.basePackage("com.echo.boot.controller")).paths(PathSelectors.any()).build();
    }

    public ApiInfo apiInfo() {
        // String title, String description, String version, String termsOfServiceUrl, Contact contact, String license, String licenseUrl, Collection<VendorExtension> vendorExtensions
        return new ApiInfoBuilder().title("springboot-Swagger2").description("Restful-API-Doc").termsOfServiceUrl("").contact(new Contact("ECHO", "http://localhost:", "869882468@qq.com")).license("1.0").build();
    }


}
