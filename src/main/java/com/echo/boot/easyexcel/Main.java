package com.echo.boot.easyexcel;

import java.math.BigDecimal;

/**
 * Created with IntelliJ IDEA
 * Created By CQ
 * Date: 2020/10/29
 * Time: 9:11
 */
public class Main {


    public static void main(String[] args) {
        bdps_gps(new BigDecimal(222.3333),new BigDecimal(33.3665));
//        gps_bdps();
    }

    static final double X_PI = Math.PI * 3000.0 / 180.0;


    // 坐标转
    static void bdps_gps(BigDecimal bd_lng, BigDecimal bd_lat) {
        double x = bd_lng.subtract(new BigDecimal("0.0065")).doubleValue();
        double y = bd_lat.subtract(new BigDecimal("0.006")).doubleValue();
        double z = Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2)) - (Math.sin(y * X_PI) * 0.00002);
        double theta = Math.atan2(y, x) - (Math.cos(x * X_PI) * 0.00003);
        double gd_lng = z * Math.cos(theta);
        double gd_lat = z * Math.sin(theta);
        System.out.println("gd_lng : " + gd_lng + ";gd_lat :" + gd_lat);
    }


    //高德坐标转百度
    static void gps_bdps(BigDecimal gd_lng, BigDecimal gd_lat) {
        double x = gd_lng.doubleValue();
        double y = gd_lat.doubleValue();
        double z = Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2)) + (Math.sin(y * X_PI) * 0.00002);
        double theta = Math.atan2(y, x) + (Math.cos(x * X_PI) * 0.00003);
        double bd_lng = z * Math.cos(theta) + 0.0065;
        double bd_lat = z * Math.sin(theta) + 0.006;
        System.out.println("bd_lng : " + bd_lng + ";bd_lat :" + bd_lat);
    }


}
