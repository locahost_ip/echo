package com.echo.boot.structure.linearlist.linked.single;

import com.echo.boot.structure.linearlist.list.MyAbstractList;

/**
 * Created with IntelliJ IDEA
 * Created By CQ
 * Date: 2020/5/3
 * Time: 21:10
 * 单向链表 虚拟头
 */
public class MySingleVirtualLinkedList<E> extends MyAbstractList<E> {
    private Node<E> first;

    public MySingleVirtualLinkedList() {
        first = new Node(null, null);
    }

    private static class Node<E> {
        // 每个节点储存里两个元素
        // 一个是信息，一个是下一个节点的地址
        E element;
        Node<E> next;

        public Node(E element, Node<E> node) {
            this.element = element;
            this.next = node;
        }
    }


    @Override
    public void clear() {
        size = 0;


    }

    @Override
    public E get(int index) {
        return null;
    }

    @Override
    public E set(int index, E element) {
        return null;
    }

    @Override
    public void add(int index, E element) {

    }

    @Override
    public E remove(int index) {
        return null;
    }

    @Override
    public int indexOf(E element) {
        return 0;
    }
}
