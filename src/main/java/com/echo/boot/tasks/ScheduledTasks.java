package com.echo.boot.tasks;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created with IntelliJ IDEA
 * Created By CQ
 * Date: 2020/4/4
 * Time: 17:39
 */
@Component
public class ScheduledTasks {

    private static final SimpleDateFormat dataForat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @Scheduled(fixedRate = 5000)
    public void reportCurrentTiem() {
        System.out.println("现在时间 ： " + dataForat.format(new Date()));
    }
}
