package com.echo.boot.structure.linearlist.queues.queue;

import com.echo.boot.structure.linearlist.linked.doublelinkedlist.MyLinkedList;
import com.echo.boot.structure.linearlist.list.MyList;

/**
 * Created with IntelliJ IDEA
 * Created By CQ
 * Date: 2020/5/7
 * Time: 22:06
 * 队列
 * 从  rear 队尾加入元素 enQueue
 * 从  front 对头取出元素 deQueue
 * first in first out
 * FIFO
 *
 * @author CQ
 */
public class MyQueue<E> {
    private MyList<E> list = new MyLinkedList<>();

    public int size() {
        return list.size();
    }

    public boolean isEmpty() {
        return list.isEmpty();
    }

    public void enQueue(E element) {
        list.add(element);
    }

    public E deQueue() {
        return list.remove(0);
    }

    public E front() {
        return list.get(0);
    }

    public void clear() {
        list.clear();
    }

}
