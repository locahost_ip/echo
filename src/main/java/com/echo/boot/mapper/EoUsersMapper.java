package com.echo.boot.mapper;

import com.echo.boot.pojo.EoUsers;

 /**
 *Created with IntelliJ IDEA
 *Created By CQ
 *Date: 2020/11/4
 *Time: 10:33 
 */
public interface EoUsersMapper {
    int deleteByPrimaryKey(Object id);

    int insert(EoUsers record);

    int insertSelective(EoUsers record);

    EoUsers selectByPrimaryKey(Object id);

    int updateByPrimaryKeySelective(EoUsers record);

    int updateByPrimaryKey(EoUsers record);
}