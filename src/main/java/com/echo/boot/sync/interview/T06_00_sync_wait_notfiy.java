package com.echo.boot.sync.interview;

/**
 * Created with IntelliJ IDEA
 * Created By CQ
 * Date: 2020/4/16
 * Time: 22:09
 */
public class T06_00_sync_wait_notfiy {
//    private static CountDownLatch latch = new CountDownLatch(1);

    public static void main(String[] args) {
        Object o = new Object();
        char[] aI = "ABCDEFG".toCharArray();
        char[] aC = "1234567".toCharArray();

        new Thread(() -> {
//            try {
//                latch.await();
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
            synchronized (o) {
                for (char c : aI) {
                    System.out.println(c);
                    try {
                        o.notify();
                        o.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                o.notify();
            }
        }, "t1").start();

        new Thread(() -> {
//            latch.countDown();
            synchronized (o) {
                for (char c : aC) {
                    System.out.println(c);
                    try {
                        o.notify();
                        o.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                o.notify();
            }
        }, "t2").start();


    }
}
