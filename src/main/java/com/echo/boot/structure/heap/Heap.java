package com.echo.boot.structure.heap;

/**
 * Created with IntelliJ IDEA
 * Created By CQ
 * Date: 2020/6/13
 * Time: 10:36
 */
public interface Heap<E> {
    int size();

    boolean isEmpty();

    void clear();

    void add(E element);

    /**
     * @author CQ
     * @DESCRIPTION: 获取堆顶元素
     * @params: []
     * @return: E
     * @Date: 2020/6/13 10:37
     * @Modified By:
     */
    E get();

    /**
     * @author CQ
     * @DESCRIPTION: 删除堆顶元素
     * @params: []
     * @return: E
     * @Date: 2020/6/13 10:37
     * @Modified By:
     */
    E remove();

    /**
     * @author CQ
     * @DESCRIPTION: 删除堆顶元素插入
     * @params: [element]
     * @return: E
     * @Date: 2020/6/13 10:38
     * @Modified By:
     */
    E replace(E element);
}

