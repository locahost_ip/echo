package com.echo.boot.mapper;

import com.echo.boot.pojo.Theme;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ThemeMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Theme record);

    int insertSelective(Theme record);

    Theme selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Theme record);

    int updateByPrimaryKey(Theme record);
}