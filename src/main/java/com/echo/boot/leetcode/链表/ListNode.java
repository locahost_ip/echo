package com.echo.boot.leetcode.链表;

/**
 * Created with IntelliJ IDEA
 * Created By CQ
 * Date: 2020/5/2
 * Time: 20:18
 */
public class ListNode {

    int val;
    ListNode next;

    public ListNode(int x) {
        val = x;
    }

    @Override
    public String toString() {
        return "ListNode{" +
                "val=" + val +
                '}';
    }
}
