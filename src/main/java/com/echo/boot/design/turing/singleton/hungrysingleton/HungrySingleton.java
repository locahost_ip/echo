package com.echo.boot.design.turing.singleton.hungrysingleton;

/**
 * Created with IntelliJ IDEA
 * Created By CQ
 * Date: 2020/7/26
 * Time: 8:49
 *
 * @author Administrator
 * * 指令重排
 * * 类加载机制
 * * JVM 序列化知识
 * * 单例模式在spring框架 & JDK源码运用
 * <p>
 * 饿汉模式 类加载的初步阶段就完成了,本质上就是借助JVM 类加载机制,保证实例的唯一性
 */
public class HungrySingleton {

    public static void main(String[] args) {
        // 多线程的情况下,也可以保证单例。
        new Thread(() -> {
            HungrySingleton instance = HungrySingleton.getInstance();
            System.out.println(instance);
        }).start();

        new Thread(() -> {
            HungrySingleton instance1 = HungrySingleton.getInstance();
            System.out.println(instance1);
        }).start();
    }

    private static HungrySingleton instance = new HungrySingleton();

    private HungrySingleton() {
    }

    public static HungrySingleton getInstance() {
        return instance;
    }
}
