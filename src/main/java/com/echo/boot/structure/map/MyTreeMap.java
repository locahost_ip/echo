package com.echo.boot.structure.map;


import java.util.Comparator;
import java.util.LinkedList;
import java.util.Objects;
import java.util.Queue;

/**
 * Created with IntelliJ IDEA
 * Created By CQ
 * Date: 2020/6/2
 * Time: 21:25
 * 自己就是一个红黑树
 * key 必须具有可比较性
 * 元素的分布是有顺序的
 * 复杂度分析
 * 底层红黑树 所以 添加，删除，搜索 O(logn);
 */
public class MyTreeMap<K, V> implements Map<K, V> {
    private static final boolean RED = false;
    private static final boolean BLACK = true;
    private Comparator<K> comparator;

    /**
     * 元素数量
     */
    private int size;
    /**
     * 根节点
     */
    private Node<K, V> root;

    public MyTreeMap() {
        this(null);
    }

    public MyTreeMap(Comparator<K> comparator) {
        this.comparator = comparator;
    }


    private static class Node<K, V> {
        K key;
        V value;
        boolean color = RED;

        Node<K, V> left;
        Node<K, V> right;
        Node<K, V> parent;

        public Node(K key, V value, Node<K, V> parent) {
            this.key = key;
            this.value = value;
            this.parent = parent;
        }

        public boolean isLeaf() {
            return left == null && right == null;
        }

        public boolean hasTwoChildren() {
            return left != null && right != null;
        }

        // 判断自己是不是左子树
        public boolean isLeftChild() {
            return parent != null && this == parent.left;
        }

        // 判断自己是不是右子树
        public boolean isRightChild() {
            return parent != null && this == parent.right;
        }

        // 返回父节点的兄弟节点

        public Node<K, V> sibling() {
            if (isLeftChild()) {
                return parent.right;
            }
            if (isRightChild()) {
                return parent.left;
            }
            return null;
        }

        @Override
        public String toString() {
            String parentString = null;
            String color = null;
            if (parent != null) {
                parentString = parent.key.toString();
            }
            if (this.color) {
                color = "black";
            } else {
                color = "red";
            }
            return key + "_p(" + parentString + ")_color(" + color + ")" + value;
        }

    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public void clear() {
        root = null;
        size = 0;
    }

    /**
     * @author CQ
     * @DESCRIPTION: 添加时返回 原来覆盖的的元素
     * @params:
     * @return:
     * @Date: 2020/6/3 10:10
     * @Modified By:
     */
    @Override
    public V put(K key, V value) {
        keyNotNullCheck(key);
        // 添加第一个元素
        if (root == null) {
            root = createNode(key, value, null);
            size++;
            afterPut(root);
            return null;
        }
        Node<K, V> parent = root;
        Node<K, V> node = root;
        int cmp = 0;
        do {
            parent = node;
            cmp = compareTo(key, node.key);
            if (cmp < 0) {
                node = node.left;
            } else if (cmp > 0) {
                node = node.right;
            } else {
                // 相等的覆盖.
                V oldValue = node.value;
                node.key = key;
                node.value = value;
                return oldValue;
            }
        } while (node != null);
        Node<K, V> newNode = createNode(key, value, parent);
        if (cmp < 0) {
            parent.left = newNode;
        } else {
            parent.right = newNode;
        }
        size++;
        afterPut(newNode);
        // 添加新节点 返回空
        return null;
    }

    protected Node<K, V> createNode(K key, V value, Node<K, V> parent) {
        return new Node<>(key, value, parent);
    }

    private int compareTo(K k1, K k2) {
        if (comparator != null) {
            return comparator.compare(k1, k2);
        }
        return ((Comparable<K>) k1).compareTo(k2);
    }

    private void keyNotNullCheck(K key) {
        if (key == null) {
            throw new IllegalArgumentException("key must not be null");
        }
    }

    private void afterPut(Node<K, V> node) {
        Node<K, V> parent = node.parent;

        // 添加的是根节点,上溢到了根节点
        if (parent == null) {
            black(node);
            return;
        }
        // 父节点是黑色,直接返回
        if (isBlack(parent)) {
            return;
        }
        //uncle 节点判断
        Node<K, V> uncle = parent.sibling();
        // 祖父节点
        Node<K, V> grand = parent.parent;
        // 红黑树 与 4 阶 B 树等价
        // 如果uncle节点是红色,就出现上溢,出现上溢中间节点上移,分裂出2个子节点,最高的情况一直上溢到根节点
        // 保证红黑树属性,子节点必须为黑色(因为只有黑色才能独立一个子节点)
        // 向上移动的节点,当成一个新添加的节点,上移,再次重复此操作。
        // 上溢的情况(也就是添加的时候uncle 是红色的时候)
        if (isRed(uncle)) {
            black(parent);
            black(uncle);
            red(grand);
            afterPut(grand);
            return;
        }
        // uncle 不是 红色
        // L
        if (parent.isLeftChild()) {
            if (node.isLeftChild()) {
                //LL
                black(parent);
                red(grand);
                rotateRight(grand);
            } else {
                //LR
                black(node);
                red(grand);
                rotateLeft(parent);
                rotateRight(grand);
            }
        } else {
            // R
            if (node.isLeftChild()) {
                // RL
                black(node);
                red(grand);
                rotateRight(parent);
                rotateLeft(grand);
            } else {
                // RR
                black(parent);
                red(grand);
                rotateLeft(grand);
            }
        }
    }

    private void rotateLeft(Node<K, V> grand) {
        Node<K, V> parent = grand.right;
        Node<K, V> child = parent.left;
        grand.right = child;
        parent.left = grand;
        afterRotate(grand, parent, child);

    }

    private void rotateRight(Node<K, V> grand) {
        Node<K, V> parent = grand.left;
        Node<K, V> child = parent.right;
        grand.left = child;
        parent.right = grand;
        afterRotate(grand, parent, child);
    }


    private void afterRotate(Node<K, V> grand, Node<K, V> parent, Node<K, V> child) {
        parent.parent = grand.parent;
        if (grand.isLeftChild()) {
            grand.parent.left = parent;
        } else if (grand.isRightChild()) {
            grand.parent.right = parent;
        } else {
            // grand是root节点
            root = parent;
        }
        // 更新child的parent
        if (child != null) {
            child.parent = grand;
        }
        grand.parent = parent;
    }

    @Override
    public V get(K key) {
        Node<K, V> node = node(key);
        return node != null ? node.value : null;
    }


    @Override
    public V remove(K key) {
//        if (key == null) {
//            return null;
//        }
        return remove(node(key));
    }

    private V remove(Node<K, V> node) {
        if (node == null) {
            return null;
        }
        Node<K, V> oldNode = node;
        size--;
        // 度为2的节点
        if (node.hasTwoChildren()) {
            // 找到后继节点
//            Node<E> s = predecessor(node);
            Node<K, V> s = successor(node);
            // 用后继节点的值覆盖度为2的节点的值
            node.key = s.key;
            node.value = s.value;
            // 删除后继节点
            node = s;
        }
        // 删除node节点( node节点必然是 1 或者 0)
        // 删除node节点（node的度必然是1或者0）
        Node<K, V> replacement = node.left != null ? node.left : node.right;
        // node是度为1的节点
        if (replacement != null) {
            // 更改parent
            replacement.parent = node.parent;
            // 更改parent的left、right的指向
            if (node.parent == null) {
                // node是度为1的节点并且是根节点
                root = replacement;
            } else if (node == node.parent.left) {
                node.parent.left = replacement;
            } else {
                // node == node.parent.right
                node.parent.right = replacement;
            }
            // 删除节点后的调整
            afterRemove(replacement);
        } else if (node.parent == null) {
            // node是叶子节点并且是根节点
            root = null;
            // 删除节点后的调整
//            afterRemove(node);
        } else {
            // node是叶子节点，但不是根节点
            if (node == node.parent.left) {
                node.parent.left = null;
            } else {
                node.parent.right = null;
            }
            // 删除节点后的调整
            afterRemove(node);

        }
        return oldNode.value;
    }

    protected void afterRemove(Node<K, V> node) {
        // 如果删除的节点是红色
        // 或者 用以取代删除节点的子节点是红色
        if (isRed(node)) {
            black(node);
            return;
        }

        Node<K, V> parent = node.parent;
        // 删除的是根节点
        if (parent == null) {
            return;
        }

        // 删除的是黑色叶子节点【下溢】
        // 判断被删除的node是左还是右
        boolean left = parent.left == null || node.isLeftChild();
        Node<K, V> sibling = left ? parent.right : parent.left;
        if (left) {
            // 被删除的节点在左边，兄弟节点在右边
            if (isRed(sibling)) {
                // 兄弟节点是红色
                black(sibling);
                red(parent);
                rotateLeft(parent);
                // 更换兄弟
                sibling = parent.right;
            }

            // 兄弟节点必然是黑色
            if (isBlack(sibling.left) && isBlack(sibling.right)) {
                // 兄弟节点没有1个红色子节点，父节点要向下跟兄弟节点合并
                boolean parentBlack = isBlack(parent);
                black(parent);
                red(sibling);
                if (parentBlack) {
                    afterRemove(parent);
                }
            } else { // 兄弟节点至少有1个红色子节点，向兄弟节点借元素
                // 兄弟节点的左边是黑色，兄弟要先旋转
                if (isBlack(sibling.right)) {
                    rotateRight(sibling);
                    sibling = parent.right;
                }

                color(sibling, colorOf(parent));
                black(sibling.right);
                black(parent);
                rotateLeft(parent);
            }
        } else { // 被删除的节点在右边，兄弟节点在左边
            if (isRed(sibling)) { // 兄弟节点是红色
                black(sibling);
                red(parent);
                rotateRight(parent);
                // 更换兄弟
                sibling = parent.left;
            }

            // 兄弟节点必然是黑色
            if (isBlack(sibling.left) && isBlack(sibling.right)) {
                // 兄弟节点没有1个红色子节点，父节点要向下跟兄弟节点合并
                boolean parentBlack = isBlack(parent);
                black(parent);
                red(sibling);
                if (parentBlack) {
                    afterRemove(parent);
                }
            } else { // 兄弟节点至少有1个红色子节点，向兄弟节点借元素
                // 兄弟节点的左边是黑色，兄弟要先旋转
                if (isBlack(sibling.left)) {
                    rotateLeft(sibling);
                    sibling = parent.left;
                }

                color(sibling, colorOf(parent));
                black(sibling.left);
                black(parent);
                rotateRight(parent);
            }
        }
    }

    private Node<K, V> node(K key) {
        Node<K, V> node = root;
        while (node != null) {
            int cmp = compareTo(key, node.key);
            if (cmp == 0) {
                return node;
            }
            if (cmp > 0) {
                node = node.right;
            }
            if (cmp < 0) {
                node = node.left;
            }
        }
        return null;
    }

    @Override
    public boolean containsKey(K key) {
        return node(key) != null;
    }

    @Override
    public boolean containsValue(V value) {
        if (root == null) {
            return false;
        }
        Queue<Node<K, V>> queue = new LinkedList<>();
        queue.offer(root);
        while (!queue.isEmpty()) {
            Node<K, V> node = queue.peek();
            if (valEquals(value, node.value)) {
                return true;
            }
            if (node.left != null) {
                queue.offer(node.left);
            }
            if (node.left != null) {
                queue.offer(node.right);
            }
        }
        return false;
    }

    private boolean valEquals(V v1, V v2) {
        return Objects.equals(v1, v2);
//        return (v1 == v2) || (v1 != null && v1.equals(v2));
    }

    @Override
    public void traversal(MapVisitor<K, V> visitor) {
        inorderTraversal(visitor);
    }

    private Node<K, V> color(Node<K, V> node, boolean color) {
        if (node == null) {
            return node;
        }
        node.color = color;
        return node;
    }

    private Node<K, V> red(Node<K, V> node) {
        return color(node, RED);
    }

    private Node<K, V> black(Node<K, V> node) {
        return color(node, BLACK);
    }

    private boolean colorOf(Node<K, V> node) {
        return node == null ? BLACK : node.color;
    }

    private boolean isRed(Node<K, V> node) {
        return colorOf(node) == RED;
    }

    private boolean isBlack(Node<K, V> node) {
        return colorOf(node) == BLACK;
    }

    /**
     * @author CQ
     * @DESCRIPTION: 前序遍历; 前序遍历的作用 : 树状结构展示(注意左右子树的顺序);
     * @DESCRIPTION: 前序遍历:先根节点, 左子树, 最后右子树;
     * @params: 策略模式： 提供外部访问树的元素
     * @return:
     * @Date: 2020/5/18 7:35
     * @Modified By:
     */
    public void preloaderTraversal(MapVisitor<K, V> visitor) {
        if (checkVisitor(visitor)) {
            return;
        }
        preloaderTraversal(root, visitor);
    }


    private void preloaderTraversal(Node<K, V> node, MapVisitor<K, V> visitor) {
        if (node == null) {
            return;
        }
        if (visitor.stop) {
            return;
        }
        visitor.stop = visitor.visit(node.key, node.value);
        preloaderTraversal(node.left, visitor);
        preloaderTraversal(node.right, visitor);
    }

    /**
     * @author CQ
     * @DESCRIPTION: 中序遍历 （二叉搜索树的中序遍历是升序或降序）
     * @DESCRIPTION: 中序遍历 : 先左子树,根节点,后右子树.
     * @params: [visitor] 提供外部访问树的元素
     * @return: void
     * @Date: 2020/5/18 7:39
     * @Modified By:
     */
    public void inorderTraversal(MapVisitor<K, V> visitor) {
        if (visitor == null) {
            return;
        }
        inorderTraversal(root, visitor);
    }

    public void inorderTraversal(Node<K, V> node, MapVisitor<K, V> visitor) {
        if (node == null) {
            return;
        }
        if (visitor.stop) {
            return;
        }
        inorderTraversal(node.left, visitor);
        if (visitor.stop) {
            return;
        }
        visitor.stop = visitor.visit(node.key, node.value);
        inorderTraversal(node.right, visitor);
    }

    /**
     * @author CQ
     * @DESCRIPTION: 后序遍历, 先左子节点, 右节点, 最后根节点.
     * @params: [visitor]
     * @return: void
     * @Date: 2020/5/18 7:42
     * @Modified By:
     */
    public void postOrderTraversal(MapVisitor<K, V> visitor) {
        if (visitor == null) {
            return;
        }
        postOrderTraversal(root, visitor);
    }

    private void postOrderTraversal(Node<K, V> node, MapVisitor<K, V> visitor) {
        if (node == null) {
            return;
        }
        if (visitor.stop) {
            return;
        }
        postOrderTraversal(node.left, visitor);
        postOrderTraversal(node.right, visitor);
        if (visitor.stop) {
            return;
        }
        visitor.stop = visitor.visit(node.key, node.value);
    }

    /**
     * @author CQ
     * @DESCRIPTION: 层级遍历
     * @params: [visitor]
     * @return: void
     * @Date: 2020/5/18 7:46
     * @Modified By:
     */
    public void levelOrderTraversal(MapVisitor<K, V> visitor) {
        if (visitor == null) {
            return;
        }
        levelOrderTraversal(root, visitor);
    }

    private void levelOrderTraversal(Node<K, V> node, MapVisitor<K, V> visitor) {
        if (node == null) {
            return;
        }
        LinkedList<Node<K, V>> deque = new LinkedList<>();
        deque.offer(node);
        while (!deque.isEmpty()) {
            Node<K, V> eNode = deque.poll();
            if (visitor.visit(eNode.key, eNode.value)) {
                return;
            }
            if (eNode.left != null) {
                deque.offer(eNode.left);
            }
            if (eNode.right != null) {
                deque.offer(eNode.right);
            }
        }
    }

    private boolean checkVisitor(MapVisitor<K, V> visitor) {
        if (visitor == null) {
            return true;
        }
        return false;
    }

    /**
     * @author CQ
     * @DESCRIPTION: 返回前驱结点(中序遍历的前一个节点)
     * @params: []
     * @return: 前序节点
     * @Date: 2020/5/18 15:50
     * @Modified By:
     */
    protected Node<K, V> predecessor(Node<K, V> node) {
        if (node == null) {
            return null;
        }
        // 前驱节点在左子树中(left.right.right.right....)
        if (node.left != null) {
            // 左子树不为空,则找到它的最右节点
            Node<K, V> p = node.left;
            while (p.right != null) {
                p = p.right;
            }
            return p;
        }
        // 能来到这里说明左子树为空
        // 跳出循环条件
        // 当父节点不为空，则顺着父节点找，直到找到【某结点为父节点的右子节点】时
        while (node.parent != null && node.parent.left == node) {
            node = node.parent;
        }
        // 来到这里有以下两种情况：
        // node.parent == null	无前驱，说明是根结点
        // node.parent.right == node 找到【某结点为父节点的右子节点】
        return node.parent;
    }

    /**
     * @author CQ
     * @DESCRIPTION: 返回后驱结点(中序遍历的后一个节点)
     * @params: []
     * @return: 后序节点
     * @Date: 2020/5/18 15:50
     * @Modified By:
     */
    protected Node<K, V> successor(Node<K, V> node) {
        if (node == null) {
            return null;
        }
        if (node.right != null) {
            Node<K, V> p = node.right;
            while (p.left != null) {
                p = p.left;
            }
            return p;
        }
        // 来到这里说明没有右节点
        // 跳出循环条件
        // 当父节点不为空，则顺着父节点找，直到找到【某结点为父节点的左子节点】时
        while (node.parent != null && node.parent.right == node) {
            node = node.parent;
        }
        // 来到这里有以下两种情况：
        // node.parent == null   无后驱，说明是根结点
        // node.parent.left == node  找到【某结点为父节点的左子节点】
        return node.parent;

    }

    @Override
    public Object root() {
        return root;
    }

    @Override
    public Object left(Object node) {
        return ((Node<K, V>) node).left;
    }

    @Override
    public Object right(Object node) {
        return ((Node<K, V>) node).right;
    }

    @Override
    public Object string(Object node) {
        return ((Node<K, V>) node);
    }

}
